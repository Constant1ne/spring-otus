package me.kostya.services;

import me.kostya.domain.TestData;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class CsvTestReaderTest {
  private static Properties props;

  @BeforeAll
  static void setUp() throws IOException {
    props = new Properties();

    InputStream is = ClassLoader.getSystemResourceAsStream("app.properties");
    props.load(is);
  }

  @Test
  void readTest() {
    TestReader testReader = new CsvTestReader(props.getProperty("test.file"));
    TestData testData = testReader.readTest();

    assertEquals(5, testData.getQuestions().size());
  }
}