package me.kostya.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class TestServiceTest {

  private ByteArrayOutputStream outContent;
  private static final PrintStream originalOut = System.out;

  private static final Locale locale =
          new Locale.Builder().setLanguage("ru").setRegion("RU").build();

  private static ReloadableResourceBundleMessageSource ms;

  private TestService testService;

  private static Properties props;

  @BeforeAll
  static void setUpAll() throws IOException {
    props = new Properties();

    InputStream is = ClassLoader.getSystemResourceAsStream("app.properties");
    props.load(is);

    ms = new ReloadableResourceBundleMessageSource();
    ms.setBasename("/bundle");
    ms.setDefaultEncoding("UTF-8");
  }

  @BeforeEach
  void setUp() {
    TestReader testReader = new CsvTestReader(props.getProperty("test.file"));

    outContent = new ByteArrayOutputStream();
    System.setOut(new PrintStream(outContent));

    testService = new TestService(testReader, ms);
    testService.setLocale(locale);
  }

  @AfterEach
  void tearDown() {
    System.setOut(originalOut);
  }

  @Test
  void runTestAllCorrect() {
    String userInput = "Pupkin\n" +
            "Vasya\n" +
            "0\n" +
            "0\n" +
            "1 3\n" +
            "2\n" +
            "0";

    String result = runTestWithUserInput(userInput);

    assertEquals("Pupkin Vasya вы набрали 5 очков", result);
  }

  @Test
  void runTest1Error() {
    String userInput = "Pupkin\n" +
            "Vasya\n" +
            "0\n" +
            "0\n" +
            "1 3\n" +
            "2\n" +
            "1";

    String result = runTestWithUserInput(userInput);

    assertEquals("Pupkin Vasya вы набрали 4 очков", result);
  }


  @Test
  void runTestMultipleChoiceError() {
    String userInput = "Pupkin\n" +
            "Vasya\n" +
            "0\n" +
            "0\n" +
            "1\n" +
            "2\n" +
            "0";

    String result = runTestWithUserInput(userInput);

    assertEquals("Pupkin Vasya вы набрали 4 очков", result);
  }

  private String runTestWithUserInput(String userInput) {
    InputStream inputStream = new ByteArrayInputStream(userInput.getBytes());
    testService.setInputStream(inputStream);

    testService.runTest();

    List<String> outputStrings = Arrays.asList(outContent.toString().split("\r\n"));

    return outputStrings.get(outputStrings.size() - 1);
  }
}