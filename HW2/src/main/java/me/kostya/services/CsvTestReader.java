package me.kostya.services;
import me.kostya.domain.TestData;
import me.kostya.domain.TestQuestion;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service
public class CsvTestReader implements TestReader {
  private final String testSource;

  public CsvTestReader(@Value("${test.file}")String testSource) {
    this.testSource = testSource;
  }

  public TestData readTest() {
    try (Stream<String> stream = Files.lines(Paths.get(testSource))) {
      List<TestQuestion> questions = stream
              .map(CsvTestReader::readQuestion)
              .collect(Collectors.toList());

      return new TestData(questions);
    } catch (IOException ex) {
      throw new IllegalArgumentException(ex);
    }
  }


  private static TestQuestion readQuestion(String str) {
    List<String> strs = Arrays.asList(str.split(";"));

    String questionText = strs.get(0);

    Set<Integer> correctAnswers = Arrays.stream(strs.get(1).split(" "))
            .map(Integer::valueOf)
            .collect(Collectors.toSet());

    List<String> answers = strs.subList(2, strs.size());

    return TestQuestion.createQuestion(questionText, answers, correctAnswers);
  }
}
