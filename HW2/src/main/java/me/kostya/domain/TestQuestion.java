package me.kostya.domain;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public class TestQuestion {
  private String questionText;
  private List<String> answers;
  private Set<Integer> correctAnswers;

  public static TestQuestion createQuestion(String questionText,
                                            List<String> answers,
                                            Set<Integer> correctAnswers) {
    TestQuestion testQuestion = new TestQuestion();
    testQuestion.questionText = questionText;
    testQuestion.answers = answers;
    testQuestion.correctAnswers = correctAnswers;
    return testQuestion;
  }

  public Collection<Integer> getCorrectAnswers() {
    return correctAnswers;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder()
            .append("\r\n\r\nВопрос: ")
            .append(questionText);

    for (int i = 0; i < answers.size(); i++) {
      sb.append("\r\n").append(i).append(") ").append(answers.get(i));
    }

    return sb.toString();
  }
}
