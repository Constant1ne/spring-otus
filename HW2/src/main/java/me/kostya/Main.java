package me.kostya;

import me.kostya.services.TestService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import java.util.Locale;

@ComponentScan
@PropertySource("classpath:app.properties")
@Configuration
public class Main {
  public static void main(String[] args) {
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
    context.register(Main.class);
    context.refresh();

    TestService service = context.getBean(TestService.class);

    service.runTest();
  }

  @Bean
  public MessageSource messageSource() {
    ReloadableResourceBundleMessageSource ms = new ReloadableResourceBundleMessageSource();
    ms.setBasename("/bundle");
    ms.setDefaultEncoding("UTF-8");
    ms.setUseCodeAsDefaultMessage(true);
    return ms;
  }

  @Bean
  public Locale locale(@Value("${locale.language}") String localeLanguage,
                       @Value("${locale.region}") String localeRegion) {

    return new Locale.Builder().setLanguage(localeLanguage).setRegion(localeRegion).build();
  }
}
