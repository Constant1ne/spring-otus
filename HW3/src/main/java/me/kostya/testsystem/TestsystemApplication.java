package me.kostya.testsystem;

import me.kostya.testsystem.services.TestService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import java.util.Locale;

@SpringBootApplication
public class TestsystemApplication {

	public static void main(String[] args) {
    ApplicationContext ctx = SpringApplication.run(TestsystemApplication.class, args);

    TestService testService = ctx.getBean(TestService.class);

    testService.runTest();
	}


  @Bean
  public MessageSource messageSource() {
    ReloadableResourceBundleMessageSource ms = new ReloadableResourceBundleMessageSource();
    ms.setBasename("/bundle");
    ms.setDefaultEncoding("UTF-8");
    ms.setUseCodeAsDefaultMessage(true);
    return ms;
  }

  @Bean
  public Locale locale(ConfigProperties config) {

    return new Locale.Builder()
            .setLanguage(config.getLanguage())
            .setRegion(config.getRegion())
            .build();
  }
}
