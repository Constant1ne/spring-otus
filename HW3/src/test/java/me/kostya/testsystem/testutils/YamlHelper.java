package me.kostya.testsystem.testutils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import me.kostya.testsystem.ConfigProperties;

import java.io.IOException;
import java.io.InputStream;

public class YamlHelper {
  public static ConfigProperties readYamlConfigProperties(String filePath) throws IOException {
    ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

    InputStream is = ClassLoader.getSystemResourceAsStream(filePath);
    JsonNode appNode = mapper.readTree(is).get("application");
    ConfigProperties config = mapper.treeToValue(appNode, ConfigProperties.class);

    return config;
  }
}
