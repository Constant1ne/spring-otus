package me.kostya.testsystem.services;

import me.kostya.testsystem.ConfigProperties;
import me.kostya.testsystem.domain.TestData;
import me.kostya.testsystem.testutils.YamlHelper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class CsvTestReaderTest {
  private static ConfigProperties config;

  @BeforeAll
  static void setUp() throws IOException {
    config = YamlHelper.readYamlConfigProperties("application.yml");
  }

  @Test
  void readTest() {
    TestReader testReader = new CsvTestReader(config);
    TestData testData = testReader.readTest();

    assertEquals(5, testData.getQuestions().size());
  }
}