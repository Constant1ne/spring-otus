package me.kostya.homework11.entities;

import org.springframework.data.mongodb.core.mapping.Document;


public class Genre {
    private String name;

    public Genre() {
    }

    public Genre(String name) {
        this.name = name;
    }

    public static Genre create(String name) {
        Genre genre = new Genre();
        genre.name = name;
        return genre;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Genre{" +
                " name='" + name + '\'' +
                '}';
    }
}
