package me.kostya.homework11.dao;

import me.kostya.homework11.entities.Author;
import me.kostya.homework11.entities.Book;
import me.kostya.homework11.entities.Genre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import reactor.core.publisher.Flux;


import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.unwind;

public class CustomBookDaoImpl implements CustomBookDao {

    private ReactiveMongoTemplate mongoTemplate;

    public CustomBookDaoImpl(@Autowired ReactiveMongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    //Агрегация не заработала, как ни пытался.
    //Возвращает незаполненные объекты вида {name: null}
    @Override
    public Flux<Genre> findAllGenres() {
//        return mongoTemplate.aggregateAndReturn(Genre.class)
//                .by(newAggregation(Book.class, unwind("genre"), group("genre")))
//                .all();

//        return mongoTemplate.aggregateAndReturn(Genre.class)
//                .inCollection("book")
//                .by(newAggregation(unwind("genre"), group("genre")))
//                .all();

        return mongoTemplate.aggregate(newAggregation(
                unwind("genre"),
                group("genre.name")), "book", Genre.class);
    }

//    @Override
//    public Flux<Author> findAllAuthors() {
//        return mongoTemplate.aggregate(newAggregation(
//                unwind("authors"),
//                group("authors")), "book", Author.class);
//    }
}
