package me.kostya.homework11.dao;

import me.kostya.homework11.entities.Book;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface BookDao extends ReactiveMongoRepository<Book, String>, CustomBookDao {

    @Query(value = "{'genre.name' : ?0 }")
    Flux<Book> getBooksByGenre(String genreName);

    Mono<Book> findFirstByOrderByIdDesc();

}
