package me.kostya.homework11.dao;

import me.kostya.homework11.entities.Author;
import me.kostya.homework11.entities.Genre;
import reactor.core.publisher.Flux;

public interface CustomBookDao {
    Flux<Genre> findAllGenres();
    //Flux<Author> findAllAuthors();
}
