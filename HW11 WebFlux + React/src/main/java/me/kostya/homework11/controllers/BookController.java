package me.kostya.homework11.controllers;

import me.kostya.homework11.dao.BookDao;
import me.kostya.homework11.entities.Book;
import me.kostya.homework11.entities.Genre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@RestController
@RequestMapping("/api")
public class BookController {
    private final BookDao bookDao;

    public BookController(@Autowired BookDao bookDao) {
        this.bookDao = bookDao;
    }

    @GetMapping("/")
    public Flux<Book> list() {
        return bookDao.findAll();
    }

    @GetMapping("/read/{id}")
    public Mono<ResponseEntity<Book>> read(@PathVariable("id") String id) {
        return bookDao.findById(id)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping("/update")
    public Mono<Book> update(@RequestBody Book book) {
        return bookDao.save(book);
    }

    @DeleteMapping("/delete")
    public Mono<Void> delete(@RequestParam("id") String id) {
        return bookDao.deleteById(id);
    }


    //просто попробовал server side events
    //UI должен показывать алерты на каждое событие
    @GetMapping(value = "/lastbookevent", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Book> getBookSaleEvents() {
        return Flux.interval(Duration.ofSeconds(15))
                .flatMap(i -> bookDao.findFirstByOrderByIdDesc());
    }

    //агрегация с помощью ReactiveMongoTemplate не заработала
    @GetMapping("/genres")
    public Flux<Genre> listGenres() {
        return bookDao.findAllGenres();
    }

    @GetMapping("/booksbygenre/{genreName}")
    public Flux<Book> booksByGenreName(@PathVariable("genreName") String genreName) {
        return bookDao.getBooksByGenre(genreName);
    }
}
