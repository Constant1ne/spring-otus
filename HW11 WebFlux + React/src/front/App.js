import React, {Component} from 'react';
import './App.css';

import BookList from "./components/BookList";
import Book from "./components/Book";
import {Route, BrowserRouter} from "react-router-dom";

class App extends Component {
    constructor(props) {
        super();

        this.eventSource = new EventSource("/api/lastbookevent");
    }

    componentDidMount() {
        this.eventSource.onmessage = e => alert(`Server Side Event (every 15 seconds) Название последней книги: ${JSON.parse(e.data).name}`);
    }

    render() {
        return (
            <div className="App">
                <BrowserRouter>
                    <div>
                        <Route exact path="/" component={BookList}/>
                        <Route path="/create" component={Book}/>
                        <Route path="/book/:id" component={Book}/>
                    </div>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;
