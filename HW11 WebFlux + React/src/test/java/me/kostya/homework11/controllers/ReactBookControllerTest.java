package me.kostya.homework11.controllers;

import me.kostya.homework11.dao.BookDao;
import me.kostya.homework11.entities.Book;
import me.kostya.homework11.entities.Genre;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.FluxExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class ReactBookControllerTest {

    private WebTestClient webTestClient;

    private BookDao bookDao;

    public ReactBookControllerTest(@Autowired BookDao bookDao) {
        this.bookDao = bookDao;
    }

    @BeforeEach
    void setUp() {
        webTestClient = WebTestClient.bindToController(new BookController(bookDao))
                .configureClient()
                .baseUrl("/api")
                .responseTimeout(Duration.ofSeconds(20))
                .build();

        bookDao.deleteAll().block();
        bookDao.save(Book.create("id123", "War and Peace", Genre.create("Novel"))).block();
    }

    @Test
    void list() {
        webTestClient.get().uri("/").exchange()
                .expectStatus().isOk()
                .expectBodyList(Book.class).hasSize(1);
    }

    @Test
    void read() {
        Book book = webTestClient.get().uri("/read/id123").exchange()
                .expectStatus().isOk()
                .expectBody(Book.class).returnResult().getResponseBody();

        assertEquals("id123",book.getId());
    }

    @Test
    void readInvalid() {
        webTestClient.get().uri("/read/zzzz").exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void update() {
        Book book = webTestClient.post().uri("/update")
                .body(Mono.just(Book.create("id123", "War and Peace2", Genre.create("Novel"))), Book.class)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectBody(Book.class)
                .returnResult().getResponseBody();

        assertEquals("War and Peace2", book.getName());
    }

    @Test
    void delete() {
        webTestClient.get().uri("/").exchange()
                .expectStatus().isOk()
                .expectBodyList(Book.class).hasSize(1);

        webTestClient.delete().uri("/delete/?id=id123")
                .exchange()
                .expectStatus().isOk();

        webTestClient.get().uri("/").exchange()
                .expectStatus().isOk()
                .expectBodyList(Book.class).hasSize(0);
    }


    //Server Side Events
    @Test
    void getBookSaleEvents() {
        FluxExchangeResult<Book> result = webTestClient.get().uri("/lastbookevent").accept(MediaType.TEXT_EVENT_STREAM)
                .exchange()
                .expectStatus().isOk()
                .returnResult(Book.class);

        StepVerifier.create(result.getResponseBody())
                .consumeNextWith(book -> assertEquals("id123", book.getId()))
                .thenCancel()
                .verify();
    }
}