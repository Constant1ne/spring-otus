package me.kostya.homework13.entities;

public class Genre {
    private String name;

    public static Genre create(String name) {
        Genre genre = new Genre();
        genre.name = name;
        return genre;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Genre{" +
                " name='" + name + '\'' +
                '}';
    }
}
