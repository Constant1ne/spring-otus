package me.kostya.homework13.dao;

import me.kostya.homework13.entities.Author;
import me.kostya.homework13.entities.Genre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;

import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.unwind;

public class CustomBookDaoImpl implements CustomBookDao {

    private MongoTemplate mongoTemplate;

    public CustomBookDaoImpl(@Autowired MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public List<Genre> findAllGenres() {
        AggregationResults<Genre> genreAggregationResults = mongoTemplate.aggregate(newAggregation(
                unwind("genre"),
                group("genre")
        ), "book", Genre.class);
        return genreAggregationResults.getMappedResults();
    }

    @Override
    public List<Author> findAllAuthors() {
        AggregationResults<Author> authorAggregationResults = mongoTemplate.aggregate(newAggregation(
                unwind("authors"),
                group("authors")), "book", Author.class);
        return authorAggregationResults.getMappedResults();
    }
}
