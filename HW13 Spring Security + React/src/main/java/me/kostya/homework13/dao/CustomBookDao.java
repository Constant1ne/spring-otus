package me.kostya.homework13.dao;

import me.kostya.homework13.entities.Author;
import me.kostya.homework13.entities.Genre;

import java.util.List;

public interface CustomBookDao {
    List<Genre> findAllGenres();
    List<Author> findAllAuthors();
}
