package me.kostya.homework13.dao;

import me.kostya.homework13.entities.Book;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface BookDao extends MongoRepository<Book, String>, CustomBookDao {

    @Query(value = "{'genre.name' : ?0 }")
    List<Book> getBooksByGenre(String genreName);

}
