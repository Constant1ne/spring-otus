class BookShelfApi {
    static fetchBookList() {
        return fetch('/api/', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "GET"
        })
            .then(response => response.json())
    }

    static fetchBook(bookId) {
        return fetch('/api/read/' + bookId, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "GET"
        })
            .then(response => response.json());
    }

    static deleteBook(bookId) {
        return fetch('/api/delete?id=' + bookId, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "DELETE"
        })
    }

    static saveBook(book) {
        return fetch('/api/update/', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: BookShelfApi.prepareToSend(book)
        });
    }

    static prepareToSend(book) {
        book.authors = book.authorsAsString.split(',').map(str => str.trim()).map(str => ({name: str}));
        return JSON.stringify(book);
    }

}

export default BookShelfApi;