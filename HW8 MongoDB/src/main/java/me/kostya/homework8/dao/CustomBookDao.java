package me.kostya.homework8.dao;

import me.kostya.homework8.entities.Author;
import me.kostya.homework8.entities.Genre;

import java.util.List;

public interface CustomBookDao {
    List<Genre> findAllGenres();
    List<Author> findAllAuthors();
}
