package me.kostya.homework8.view;

import me.kostya.homework8.entities.Author;
import me.kostya.homework8.entities.Book;
import me.kostya.homework8.entities.Comment;
import me.kostya.homework8.entities.Genre;
import me.kostya.homework8.servicies.BookshelfService;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.util.Arrays;
import java.util.stream.Collectors;

@ShellComponent
public class ShellView {

    private BookshelfService bookshelf;

    public ShellView(BookshelfService bookshelf) {
        this.bookshelf = bookshelf;
    }

    @ShellMethod("View all authors")
    public String listAuthors() {
        return bookshelf.getAllAuthors().stream()
                .map(Author::toString)
                .collect(Collectors.joining("\r\n"));
    }

    @ShellMethod("View all genres")
    public String listGenres() {
        return bookshelf.getAllGenres().stream()
                .map(Genre::toString)
                .collect(Collectors.joining("\r\n"));
    }

    @ShellMethod("View all books")
    public String listBooks() {
        return bookshelf.getAllBooks().stream()
                .map(Book::toString)
                .collect(Collectors.joining("\r\n"));
    }

    @ShellMethod("Add Book")
    public String addBook(String bookName, String genreName) {
        Book book = Book.create(bookName, genreName);
        book = bookshelf.saveBook(book);
        return "Book id is " + book.getId();
    }

    @ShellMethod("Update Book")
    public String updateBook(String bookId, String bookName, String genreName) {
        try {
            bookshelf.saveBook(Book.create(bookId, bookName, Genre.create(genreName)));
            return "OK";
        } catch (Exception ex) {
            return "Error";
        }
    }

    @ShellMethod("List Book Authors")
    public String listBookAuthors(String bookId) {
        return bookshelf.getBook(bookId).getAuthors().stream()
                .map(Author::toString)
                .collect(Collectors.joining("\r\n"));
    }

    @ShellMethod("Set Book Authors")
    public String setBookAuthors(String bookId, String... authorIds) {
        bookshelf.updateBookAuthors(bookId, Arrays.asList(authorIds));
        return "OK";
    }

    @ShellMethod("Delete Book")
    public String deleteBook(String bookId) {
        try {
            bookshelf.deleteBook(bookId);
            return "OK";
        } catch (Exception ex) {
            return "Error";
        }
    }

    @ShellMethod("List all books of genre")
    public String getAllBooksForGenre(String genreName) {
        return bookshelf.getBooksOfGenre(genreName).stream()
                .map(Book::toString)
                .collect(Collectors.joining("\r\n"));
    }

    @ShellMethod("View book")
    public String getBook(String bookId) {
        try {
            return bookshelf.getBook(bookId).toString();
        } catch (Exception ex) {
            return "ERROR";
        }
    }

    @ShellMethod("Add comment")
    public String addComment(String bookId, String commentText) {
        try {
            bookshelf.addComment(bookId, commentText);
            return "OK";
        } catch (Exception ex) {
            return "ERROR";
        }
    }

    @ShellMethod("Add author")
    public String addAuthor(String bookId, String authorName) {
        try {
            Book book = bookshelf.addAuthor(bookId, authorName);
            return book.toString();
        } catch (Exception ex) {
            return "ERROR";
        }
    }

    @ShellMethod("Set genre")
    public String setGenre(String bookId, String genreName) {
        try {
            Book book = bookshelf.setGenre(bookId, genreName);
            return book.toString();
        } catch (Exception ex) {
            return "ERROR";
        }
    }
}
