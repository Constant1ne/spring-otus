package me.kostya.homework8.servicies;

import me.kostya.homework8.entities.Author;
import me.kostya.homework8.entities.Book;
import me.kostya.homework8.entities.Comment;
import me.kostya.homework8.entities.Genre;

import java.util.List;

public interface BookshelfService {
    Book saveBook(Book book);

    List<Author> getAllAuthors();
    List<Book> getAllBooks();
    List<Genre> getAllGenres();

    Book addAuthor(String bookId, String authorName);
    Book setGenre(String bookId, String genreName);

    Book getBook(String id);

    void deleteBook(String id);

    void deleteAll();

    Book updateBookAuthors(String bookId, List<String> authorNames);

    List<Book> getBooksOfGenre(String genreName);

    Book addComment(String bookId, String commentText);
}
