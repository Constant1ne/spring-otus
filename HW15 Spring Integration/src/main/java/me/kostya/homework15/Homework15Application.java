package me.kostya.homework15;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.channel.*;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.MessageChannels;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.scheduling.PollerMetadata;
import org.springframework.messaging.Message;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@SpringBootApplication
@IntegrationComponentScan
public class Homework15Application {

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(Homework15Application.class, args);

        Upcase upcaseFlowGateway = ctx.getBean(Upcase.class);

        Collection<Person> persons = generatePersons(1_000);

        Collection<String> result = upcaseFlowGateway.upcasePersonName(persons);

        System.out.println(result);

        ctx.close();
    }

    @MessagingGateway
    public interface Upcase {
        @Gateway(requestChannel = "upcase.input")
        Collection<String> upcasePersonName(Collection<Person> person);
    }

    @Bean
    public IntegrationFlow upcase() {
        return f -> f
                .split()
                .channel(priorityChannel())
                .log()
                .channel(queueChannel())
                //.log()
                .transform(Person::getName)
                .log()
                .channel(rendezvousChannel())
                //.log()
                .channel(publishSubscribeChannel())
                //.log()
                .channel(directChannel())
                //.log()
                .<String, String>transform(String::toUpperCase)
                .aggregate();
    }

    //********************//
    //***Point to Point***//
    //********************//

    //Блокирующая очередь с буффером
    @Bean
    QueueChannel queueChannel () {
        return MessageChannels.queue(10).get();
    }

    //Блокирующая очередь с буффером и приоритетом
    @Bean
    PriorityChannel priorityChannel () {
        Comparator<Message<?>> messageComparator = Comparator.comparing(o -> ((Person) o.getPayload()).getSalary());
        return MessageChannels.priority().capacity(10).comparator(messageComparator).get();
    }

    //Блокирующая очередь без буффера
    @Bean
    RendezvousChannel rendezvousChannel () {
        return MessageChannels.rendezvous().get();
    }


    //**************************//
    //***Publish to Subscribe***//
    //**************************//

    //Вызывает всех подписчиков
    @Bean
    PublishSubscribeChannel publishSubscribeChannel () {
        return MessageChannels.publishSubscribe().get();
    }

    //Вызывает 1 подписчика по Round Robin (стратегию можно менять) т.е. получается как-бы Point to Point
    @Bean
    DirectChannel directChannel () {
        return MessageChannels.direct().get();
    }


    @Bean(name = PollerMetadata.DEFAULT_POLLER)
    PollerMetadata poller() {
        return Pollers.fixedDelay(3, TimeUnit.SECONDS).maxMessagesPerPoll(10).get();
    }


    private static Collection<Person> generatePersons(int n) {
        return IntStream.range(0, n)
                .mapToObj(i -> new Person(String.format("John%d",i), new BigDecimal(1_000_000 - i)))
                .collect(Collectors.toList());
    }
}
