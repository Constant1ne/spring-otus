package me.kostya.testsystem.domain;

import java.util.Collection;

public class TestData {
  private final Collection<TestQuestion> questions;

  public TestData(Collection<TestQuestion> questions) {
    this.questions = questions;
  }

  public Collection<TestQuestion> getQuestions() {
    return questions;
  }
}
