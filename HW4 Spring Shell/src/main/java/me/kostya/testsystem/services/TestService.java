package me.kostya.testsystem.services;

import me.kostya.testsystem.domain.TestData;
import me.kostya.testsystem.domain.TestQuestion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

@ShellComponent
public class TestService {
  private TestReader testReader;

  private Scanner scan = new Scanner(System.in);
  private final MessageSource messageSource;

  private Locale locale;

  @Autowired
  public TestService(TestReader testReader, MessageSource messageSource) {
    this.testReader = testReader;
    this.messageSource = messageSource;
  }

  @Autowired
  public void setLocale(Locale locale) {
    this.locale = locale;
  }

  void setInputStream(InputStream inputStream) {
    this.scan = new Scanner(inputStream);
  }

  @ShellMethod(value = "Run test", key = "run-test")
  public void runTest() {
    TestData test = testReader.readTest();

    System.out.println(
            messageSource.getMessage("insert.surname", new String[] {}, locale));
    String surname = scan.nextLine();

    System.out.println(
            messageSource.getMessage("insert.name", new String[] {}, locale));
    String name = scan.nextLine();

    Integer scores = 0;
    for(TestQuestion question : test.getQuestions()) {
      System.out.println(question);
      Set<Integer> answers = readAnswers();

      if(question.getCorrectAnswers().equals(answers)) {
        scores++;
      }
    }

    System.out.println(
            messageSource.getMessage("test.result",
                    new String[] {surname, name, scores.toString()}, locale));
  }

  private Set<Integer> readAnswers() {
    String answer = scan.nextLine().trim();

    return Arrays.stream(answer.split(" "))
            .map(Integer::valueOf)
            .collect(Collectors.toSet());
  }

}
