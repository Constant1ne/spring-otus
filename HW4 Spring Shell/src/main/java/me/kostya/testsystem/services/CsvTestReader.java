package me.kostya.testsystem.services;

import me.kostya.testsystem.ConfigProperties;
import me.kostya.testsystem.domain.TestData;
import me.kostya.testsystem.domain.TestQuestion;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Service
public class CsvTestReader implements TestReader {
  private final String testSource;

  public CsvTestReader(ConfigProperties config) {
    this.testSource = config.getTestFile();
  }

  public TestData readTest() {
    try (InputStream is = ClassLoader.getSystemResourceAsStream(testSource)) {
      List<TestQuestion> questions = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))
              .lines()
              .map(CsvTestReader::readQuestion)
              .collect(Collectors.toList());
      return new TestData(questions);
    } catch (IOException ex) {
      throw new IllegalArgumentException(ex);
    }
  }


  private static TestQuestion readQuestion(String str) {
    List<String> strs = Arrays.asList(str.split(";"));

    String questionText = strs.get(0);

    Set<Integer> correctAnswers = Arrays.stream(strs.get(1).split(" "))
            .map(Integer::valueOf)
            .collect(Collectors.toSet());

    List<String> answers = strs.subList(2, strs.size());

    return TestQuestion.createQuestion(questionText, answers, correctAnswers);
  }
}
