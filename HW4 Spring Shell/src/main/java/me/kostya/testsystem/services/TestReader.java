package me.kostya.testsystem.services;

import me.kostya.testsystem.domain.TestData;

public interface TestReader {
  TestData readTest();
}
