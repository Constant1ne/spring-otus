package me.kostya.testsystem.services;

import me.kostya.testsystem.ConfigProperties;
import me.kostya.testsystem.domain.TestData;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class CsvTestReaderTest {
    private static ConfigProperties config = new ConfigProperties();

    @BeforeAll
    static void setUp() throws IOException {
        config.setLanguage("ru");
        config.setRegion("RU");
        config.setTestFile("testfile.csv");
        config.setVersion("1.0");
    }

    @Test
    void readTest() {
        TestReader testReader = new CsvTestReader(config);
        TestData testData = testReader.readTest();

        assertEquals(5, testData.getQuestions().size());
    }
}