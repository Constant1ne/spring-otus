package me.kostya.testsystem.services;

import me.kostya.testsystem.ConfigProperties;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

class TestServiceTest {
    private static final PrintStream originalOut = System.out;

    private static final Locale locale =
            new Locale.Builder().setLanguage("ru").setRegion("RU").build();

    private static ReloadableResourceBundleMessageSource ms;

    private TestService testService;

    private static ConfigProperties config = new ConfigProperties();

    @BeforeAll
    static void setUpAll() throws IOException {
        config.setLanguage("ru");
        config.setRegion("RU");
        config.setTestFile("testfile.csv");
        config.setVersion("1.0");


        ms = new ReloadableResourceBundleMessageSource();
        ms.setBasename("/bundle");
        ms.setDefaultEncoding("UTF-8");
    }

    @BeforeEach
    void setUp() {
        TestReader testReader = new CsvTestReader(config);

        testService = new TestService(testReader, ms);
        testService.setLocale(locale);
    }

    @Test
    void runTestAllCorrect() {
        String userInput = "Pupkin\n" +
                "Vasya\n" +
                "0\n" +
                "0\n" +
                "1 3\n" +
                "2\n" +
                "0";

        String result = runTestWithUserInput(testService, userInput);

        assertEquals("Pupkin Vasya вы набрали 5 очков", result);
    }

    @Test
    void runTest1Error() {
        String userInput = "Pupkin\n" +
                "Vasya\n" +
                "0\n" +
                "0\n" +
                "1 3\n" +
                "2\n" +
                "1";

        String result = runTestWithUserInput(testService, userInput);

        assertEquals("Pupkin Vasya вы набрали 4 очков", result);
    }


    @Test
    void runTestMultipleChoiceError() {
        String userInput = "Pupkin\n" +
                "Vasya\n" +
                "0\n" +
                "0\n" +
                "1\n" +
                "2\n" +
                "0";

        String result = runTestWithUserInput(testService, userInput);

        assertEquals("Pupkin Vasya вы набрали 4 очков", result);
    }

    static String runTestWithUserInput(TestService testService, String userInput) {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));


        InputStream inputStream = new ByteArrayInputStream(userInput.getBytes());
        testService.setInputStream(inputStream);

        testService.runTest();

        List<String> outputStrings = Arrays.asList(outContent.toString().split("\n"));

        System.setOut(originalOut);

        return outputStrings.get(outputStrings.size() - 1);
    }
}