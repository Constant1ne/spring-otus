package me.kostya.testsystem.services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.shell.jline.InteractiveShellApplicationRunner;
import org.springframework.shell.jline.ScriptShellApplicationRunner;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static me.kostya.testsystem.services.TestServiceTest.runTestWithUserInput;
import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * Test with simulation of user input
 * With spring context
 * <p>
 * Logic is the same as in {@link me.kostya.testsystem.services.TestServiceTest}
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = {
        InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED + "=false",
        ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT_ENABLED + "=false"
})
public class TestsystemApplicationTests {

    @Autowired
    private TestService testService;

    @Test
    public void runTestAllCorrect() {
        String userInput = "Pupkin\n" +
                "Vasya\n" +
                "0\n" +
                "0\n" +
                "1 3\n" +
                "2\n" +
                "0";

        String result = runTestWithUserInput(testService, userInput);

        assertEquals("Pupkin Vasya вы набрали 5 очков", result);
    }

    @Test
    public void runTest1Error() {
        String userInput = "Pupkin\n" +
                "Vasya\n" +
                "0\n" +
                "0\n" +
                "1 3\n" +
                "2\n" +
                "1";

        String result = runTestWithUserInput(testService, userInput);

        assertEquals("Pupkin Vasya вы набрали 4 очков", result);
    }


    @Test
    public void runTestMultipleChoiceError() {
        String userInput = "Pupkin\n" +
                "Vasya\n" +
                "0\n" +
                "0\n" +
                "1\n" +
                "2\n" +
                "0";

        String result = runTestWithUserInput(testService, userInput);

        assertEquals("Pupkin Vasya вы набрали 4 очков", result);
    }

}
