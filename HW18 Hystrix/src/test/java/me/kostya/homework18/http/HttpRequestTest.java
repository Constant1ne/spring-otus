package me.kostya.homework18.http;

import me.kostya.homework18.entities.Book;
import me.kostya.homework18.servicies.BookshelfService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class HttpRequestTest {
    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    BookshelfService bookshelf;

    @BeforeEach
    void init() {
        bookshelf.deleteAll();
        Book book = Book.create("Букварь","Учебник");
        book.setId("testbookId1");
        bookshelf.saveBook(book);
    }

    @Test
    void list() throws Exception {

        assertThat(this.restTemplate.getForObject("http://localhost:11111/",
                String.class)).contains("<script src=\"/static/js/main.");
    }
}
