package me.kostya.homework18.security;

import me.kostya.homework18.controllers.BookController;
import me.kostya.homework18.servicies.BookshelfService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(BookController.class)
@Import(SecurityConfig.class)
public class SecurityConfigTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private BookshelfService bookshelfService;

    @WithMockUser(username = "admin", authorities = {"ADMIN"})
    @Test
    public void deleteBookByAdminShouldBeOK() throws Exception {
        mvc.perform(delete("/api/delete").param("id","123"))
                .andExpect(status().isOk());
    }

    @WithMockUser(username = "user", authorities = {"USER"})
    @Test
    public void deleteBookByUserShouldFail() throws Exception {
        mvc.perform(delete("/api/delete").param("id","123"))
                .andExpect(status().is(403));
    }

    @Test
    public void deleteBookByAnonymousShouldFailWithAuthenticationNeeded() throws Exception {
        mvc.perform(delete("/api/delete").param("id","123"))
                .andExpect(status().is(302));
    }

    @WithMockUser(username = "user", authorities = {"USER"})
    @Test
    public void readBookByUserShouldBeOK() throws Exception {
        mvc.perform(get("/api/read/123"))
                .andExpect(status().isOk());
    }
}