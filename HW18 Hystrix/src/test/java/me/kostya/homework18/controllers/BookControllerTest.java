package me.kostya.homework18.controllers;

import me.kostya.homework18.entities.Book;
import me.kostya.homework18.entities.Genre;
import me.kostya.homework18.servicies.BookshelfService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(secure = false)
class BookControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private BookshelfService bookshelfService;

    private BookController bookController;

    public BookControllerTest(@Autowired BookController bookController) {
        this.bookController = bookController;
    }

    @BeforeEach
    void setUp() {
    }


    @Test
    void list() throws Exception {
        String content = "[{\"id\":\"123\",\"name\":\"War and Peace\",\"genre\":{\"name\":\"Poem\"},\"authors\":[],\"comments\":[],\"authorsAsString\":\"\"}]";

        given(bookshelfService.getAllBooks())
                .willReturn(Collections.singletonList(Book.create("123", "War and Peace", Genre.create("Poem"))));

        mvc.perform(get("/api/").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(content));
    }


    @Test
    void read() throws Exception {
        String content = "{\"id\":\"123\",\"name\":\"War and Peace\",\"genre\":{\"name\":\"Poem\"},\"authors\":[],\"comments\":[],\"authorsAsString\":\"\"}";

        given(bookshelfService.getBook("123"))
                .willReturn(Book.create("123", "War and Peace", Genre.create("Poem")));

        mvc.perform(get("/api/read/123").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(content));
    }

    @Test
    void update() throws Exception {
        String content = "{\"id\":\"123\",\"name\":\"War and Peace\",\"genre\":{\"name\":\"Poem\"},\"authors\":[],\"comments\":[],\"authorsAsString\":\"\"}";
        Book book = Book.create("123", "War and Peace", Genre.create("Poem"));

        given(bookshelfService.saveBook(any()))
                .willReturn(book);

        mvc.perform(post("/api/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(content));
    }
}