package me.kostya.homework18.servicies;

import me.kostya.homework18.entities.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
class BookshelfServiceImplTest {

    private static final String BOOK_NAME_ONEGIN = "Evgenu Onegin";
    private static final String BOOK_NAME_WARPEACE = "War and Peace";

    private static final String GENRE_NAME_NOVEL = "Novel";
    private static final String GENRE_NAME_POEM = "Poem";

    private static final String AUTHOR_NAME_PUSHKIN = "Pushkin";
    private static final String AUTHOR_NAME_TOLSTOY = "Tolstoy";

    @Autowired
    BookshelfService bookshelf;

    @BeforeEach
    void setUp() {
        bookshelf.deleteAll();
    }

    @Test
    void saveBook() {
        Book book = Book.create(BOOK_NAME_ONEGIN);

        bookshelf.saveBook(book);

        Book result = bookshelf.getBook(book.getId());

        assertEquals(BOOK_NAME_ONEGIN, result.getName());
    }

    @Test
    void getBook() {
        Book book = Book.create(BOOK_NAME_ONEGIN);

        bookshelf.saveBook(book);

        Book result = bookshelf.getBook(book.getId());

        assertEquals(BOOK_NAME_ONEGIN, result.getName());
    }

    @Test
    void deleteBook() {
        Book book = Book.create(BOOK_NAME_ONEGIN);

        bookshelf.saveBook(book);

        Book result = bookshelf.getBook(book.getId());

        assertEquals(BOOK_NAME_ONEGIN, result.getName());

        bookshelf.deleteBook(book.getId());

        Book hystrixResult = bookshelf.getBook(book.getId());

        assertEquals("Провал операции", hystrixResult.getName());
    }

    @Test
    void getAllAuthors() {
        Author pushkin = Author.create(AUTHOR_NAME_PUSHKIN);
        Book onegin = Book.create(BOOK_NAME_ONEGIN);
        onegin.addAuthor(pushkin);
        bookshelf.saveBook(onegin);

        Author tolstoy = Author.create(AUTHOR_NAME_TOLSTOY);
        Book wp = Book.create(BOOK_NAME_WARPEACE);
        wp.addAuthor(tolstoy);
        bookshelf.saveBook(wp);

        List<Author> allAuthors = bookshelf.getAllAuthors();

        assertEquals(2, allAuthors.size());
    }

    @Test
    void getAllBooks() {
        Book onegin = Book.create(BOOK_NAME_ONEGIN);
        bookshelf.saveBook(onegin);

        Book wp = Book.create(BOOK_NAME_WARPEACE);
        bookshelf.saveBook(wp);

        List<Book> allBooks = bookshelf.getAllBooks();

        assertEquals(2, allBooks.size());
    }

    @Test
    void getAllGenres() {
        Book onegin = Book.create(BOOK_NAME_ONEGIN, GENRE_NAME_POEM);
        bookshelf.saveBook(onegin);

        Book wp = Book.create(BOOK_NAME_WARPEACE, GENRE_NAME_NOVEL);
        bookshelf.saveBook(wp);

        List<Genre> allGenres = bookshelf.getAllGenres();

        assertEquals(2, allGenres.size());
    }

    @Test
    void addAuthor() {
        Book onegin = Book.create(BOOK_NAME_ONEGIN);
        bookshelf.saveBook(onegin);

        bookshelf.addAuthor(onegin.getId(), AUTHOR_NAME_PUSHKIN);

        Book result = bookshelf.getBook(onegin.getId());

        assertEquals(AUTHOR_NAME_PUSHKIN, result.getAuthors().toArray(new Author[1])[0].getName());
    }

    @Test
    void setGenre() {
        Book onegin = Book.create(BOOK_NAME_ONEGIN);
        bookshelf.saveBook(onegin);

        bookshelf.setGenre(onegin.getId(), GENRE_NAME_POEM);

        Book result = bookshelf.getBook(onegin.getId());

        assertEquals(GENRE_NAME_POEM, result.getGenre().getName());
    }

    @Test
    void deleteAll() {
        Book onegin = Book.create(BOOK_NAME_ONEGIN);
        bookshelf.saveBook(onegin);

        bookshelf.deleteAll();

        assertTrue(bookshelf.getAllBooks().isEmpty());
    }

    @Test
    void updateBookAuthors() {
        Book onegin = Book.create(BOOK_NAME_ONEGIN);
        bookshelf.saveBook(onegin);

        bookshelf.updateBookAuthors(onegin.getId(), Collections.singletonList(AUTHOR_NAME_PUSHKIN));

        Book result = bookshelf.getBook(onegin.getId());

        assertEquals(AUTHOR_NAME_PUSHKIN, result.getAuthors().toArray(new Author[1])[0].getName());
    }

    @Test
    void getBooksOfGenre() {
        Book onegin = Book.create(BOOK_NAME_ONEGIN, GENRE_NAME_POEM);
        bookshelf.saveBook(onegin);

        Book wp = Book.create(BOOK_NAME_WARPEACE, GENRE_NAME_NOVEL);
        bookshelf.saveBook(wp);

        List<Book> poems = bookshelf.getBooksOfGenre(GENRE_NAME_POEM);

        assertEquals(BOOK_NAME_ONEGIN, poems.get(0).getName());
    }

    @Test
    void addComment() {
        Book onegin = Book.create(BOOK_NAME_ONEGIN);
        bookshelf.saveBook(onegin);

        bookshelf.addComment(onegin.getId(), "First one");

        Book result = bookshelf.getBook(onegin.getId());

        assertEquals("First one", result.getComments().toArray(new Comment[1])[0].getText());
    }
}