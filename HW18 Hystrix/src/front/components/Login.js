import React from 'react';
import {Redirect} from 'react-router-dom';

import {withStyles} from '@material-ui/core/styles';
import Button from "@material-ui/core/Button/Button";
import Paper from '@material-ui/core/Paper';
import FormControl from "@material-ui/core/FormControl/FormControl";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Input from "@material-ui/core/Input/Input";

const styles = theme => ({
    paperStyle: {
        margin: theme.spacing.unit * 8
    }
});

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            login: '',
            password: ''
        }
    }

    render() {
        return (
            <Paper className={this.props.classes.paperStyle}>
                <div>
                    {this.props.isAuthenticated && <Redirect to='/'/>}
                    <FormControl>
                        <InputLabel htmlFor="login">Логин</InputLabel>
                        <Input id="login" value={this.state.login} onChange={this.handleChange('login')}/>
                    </FormControl>

                    <FormControl>
                        <InputLabel htmlFor="password">Пароль</InputLabel>
                        <Input id="password" value={this.state.password} onChange={this.handleChange('password')}/>
                    </FormControl>
                    <br/>
                    <Button variant="contained" color="primary" aria-label="Login"
                            onClick={(event) => this.handleClick(event)}>
                        Вход
                    </Button>
                </div>
            </Paper>
        );
    }

    handleClick(event) {
        fetch('/api/perform_login', {
            method: "POST",
            redirect: "manual",
            body: `username=${this.state.login}&password=${this.state.password}`,
            headers: {
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
            }
        })
            .then(response => {
                if(response.status === 403) {
                    alert('Вы ошиблись, а правильные логин/пароль это admin/password');
                } else {
                    this.props.authenticateUser();
                }
            });
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };
}

export default withStyles(styles)(Login);