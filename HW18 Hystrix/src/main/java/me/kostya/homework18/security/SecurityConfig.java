package me.kostya.homework18.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;


import java.util.Arrays;
import java.util.Collections;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                //.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                //.and()
                .authorizeRequests().antMatchers("/api/update/", "/api/delete").hasAuthority("ADMIN")
                .and()
                .authorizeRequests().antMatchers("/api/read").hasAuthority("USER")
                .and()
                .authorizeRequests().antMatchers("/api/").permitAll()
                .and()
                .formLogin()
                //.loginPage("/default_login_form")
                .failureHandler((httpServletRequest, httpServletResponse, e) -> httpServletResponse.setStatus(403))
                .loginProcessingUrl("/api/perform_login");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new PasswordEncoder() {
            @Override
            public String encode(CharSequence charSequence) {
                return charSequence.toString();
            }

            @Override
            public boolean matches(CharSequence charSequence, String s) {
                return charSequence.toString().equals(s);
            }
        };
    }

    @Autowired
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(this.userDetailsServiceBean());
        //.withUser("admin").password("password").roles("ADMIN");
    }

    @Override
    public UserDetailsService userDetailsServiceBean() throws Exception {
        return userName -> {
            if ("admin".equals(userName)) {
                return new User("admin", "password",
                        Arrays.asList(new SimpleGrantedAuthority("USER"), new SimpleGrantedAuthority("ADMIN")));
            } else if ("user".equals(userName)) {
                return new User("user", "password",
                        Collections.singletonList(new SimpleGrantedAuthority("USER")));
            } else {
                throw new UsernameNotFoundException(userName);
            }
        };
    }
}
