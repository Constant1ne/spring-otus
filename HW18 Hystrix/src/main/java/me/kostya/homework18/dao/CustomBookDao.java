package me.kostya.homework18.dao;

import me.kostya.homework18.entities.Author;
import me.kostya.homework18.entities.Genre;

import java.util.List;

public interface CustomBookDao {
    List<Genre> findAllGenres();
    List<Author> findAllAuthors();
}
