package me.kostya.homework18.servicies;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import me.kostya.homework18.dao.*;
import me.kostya.homework18.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class BookshelfServiceImpl implements BookshelfService {

    private BookDao bookDao;

    public BookshelfServiceImpl(@Autowired BookDao bookDao) {
        this.bookDao = bookDao;
    }

    @HystrixCommand(
            threadPoolKey = "ThreadPool[BookshelfServiceImpl].save]",
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize",     value = "2"),
                    @HystrixProperty(name = "maxQueueSize", value = "2")
            },
            fallbackMethod = "fallbackSaveBook"
    )
    @Override
    public Book saveBook(Book book) {
        return bookDao.save(book);
    }

    @HystrixCommand(
            threadPoolKey = "ThreadPool[BookshelfServiceImpl].get]",
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize",     value = "10"),
                    @HystrixProperty(name = "maxQueueSize", value = "5")
            },
            fallbackMethod = "fallbackGetBook"
    )
    @Override
    public Book getBook(String id) {
        return bookDao.findById(id).orElseThrow(NoSuchElementException::new);
    }

    @Override
    public void deleteBook(String id) {
        bookDao.deleteById(id);
    }

    @Override
    public List<Author> getAllAuthors() {
        return bookDao.findAllAuthors();
    }

    @HystrixCommand(
            threadPoolKey = "ThreadPool[BookshelfServiceImpl].list]",
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize",     value = "10"),
                    @HystrixProperty(name = "maxQueueSize", value = "5")
            },
            fallbackMethod = "fallbackListBook"
    )
    @Override
    public List<Book> getAllBooks() {
        return bookDao.findAll();
    }

    @Override
    public List<Genre> getAllGenres() {
        return bookDao.findAllGenres();
    }

    @Override
    public Book addAuthor(String bookId, String authorName) {
        Book book = bookDao.findById(bookId).orElseThrow(NoSuchElementException::new);

        book.addAuthor(Author.create(authorName));

        return bookDao.save(book);
    }

    @Override
    public Book setGenre(String bookId, String genreName) {
        Book book = bookDao.findById(bookId).orElseThrow(NoSuchElementException::new);

        book.setGenre(Genre.create(genreName));

        return bookDao.save(book);
    }

    @Override
    public void deleteAll() {
        bookDao.deleteAll();
    }

    @Override
    public Book updateBookAuthors(String bookId, List<String> authorNames) {
        Book book = bookDao.findById(bookId).orElseThrow(NoSuchElementException::new);
        book.deleteAuthors();

        authorNames.stream().map(Author::create).forEach(book::addAuthor);

        return bookDao.save(book);
    }

    @Override
    public List<Book> getBooksOfGenre(String genreName) {
        return bookDao.getBooksByGenre(genreName);
    }

    @Override
    public Book addComment(String bookId, String commentText) {
        Book book = bookDao.findById(bookId).orElseThrow(NoSuchElementException::new);

        book.addComment(Comment.create(commentText));

        return bookDao.save(book);
    }

    public Book fallbackSaveBook(Book book) {
        return Book.create("Беда", "Трагедия");
    }

    public Book fallbackGetBook(String id) {
        return Book.create("Провал операции", "Трагедия");
    }

    public List<Book> fallbackListBook() {
        return Collections.singletonList(Book.create("Полный провал", "Трагедия"));
    }
}
