package me.kostya.homework9.http;

import me.kostya.homework9.entities.Book;
import me.kostya.homework9.servicies.BookshelfService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class HttpRequestTest {
    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    BookshelfService bookshelf;

    @BeforeEach
    void init() {
        bookshelf.deleteAll();
        Book book = Book.create("Букварь","Учебник");
        book.setId("testbookId1");
        bookshelf.saveBook(book);
    }

    @Test
    void list() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:11111/",
                String.class)).contains("Список книг");
    }

    @Test
    void create() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:11111/create",
                String.class)).contains("Добавить книгу");
    }


    @Test
    void update() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:11111/update?id=testbookId1",
                String.class)).contains("Сохранить");
    }
}
