package me.kostya.homework9.dao;

import me.kostya.homework9.entities.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
class CustomBookDaoImplTest {


    @Autowired
    BookDao bookDao;

    @BeforeEach
    void setUp() {
        bookDao.deleteAll();
    }

    @Test
    void findByName() {
        Book book = Book.create("book1");
        book = bookDao.save(book);

        Book test = bookDao.findById(book.getId()).get();
        assertEquals(book.getName(), test.getName());
    }

    @Test
    void findAll() {
        Book book1 = Book.create("book1");
        Book ivan = Book.create("Ivan");

        bookDao.save(book1);
        bookDao.save(ivan);

        assertEquals(2, bookDao.findAll().size());
    }

    @Test
    void count() {
        Book book1 = Book.create("book1");
        bookDao.save(book1);

        assertEquals(1, bookDao.count());
    }

    @Test
    void insert() {
        Book book1 = Book.create("book1");
        bookDao.save(book1);

        Book test = bookDao.findById(book1.getId()).get();
        assertEquals(book1.getName(), test.getName());
    }

    @Test
    void update() {
        Book book1 = Book.create("book1");
        bookDao.save(book1);

        book1.setName("Ivan");
        bookDao.save(book1);

        Book test = bookDao.findById(book1.getId()).get();
        assertEquals(book1.getName(), test.getName());
    }

    @Test
    void deleteById() {
        Book book1 = Book.create("book1");
        bookDao.save(book1);

        bookDao.deleteById(book1.getId());

        Book test = bookDao.findById(book1.getId()).orElse(null);
        assertNull(test);
    }

    @Test
    void findAllGenres() {
        Book onegin = Book.create("Onegin", "Poem");
        bookDao.save(onegin);

        Book wp = Book.create("War and peace", "Novel");
        bookDao.save(wp);

        List<Genre> allGenres = bookDao.findAllGenres();

        assertEquals(2, allGenres.size());
    }

    @Test
    void findAllAuthors() {
        Author pushkin = Author.create("Pushkin");
        Book onegin = Book.create("Onegin");
        onegin.addAuthor(pushkin);
        bookDao.save(onegin);

        Author tolstoy = Author.create("Tolstoy");
        Book wp = Book.create("war and peace");
        wp.addAuthor(tolstoy);
        bookDao.save(wp);

        List<Author> allAuthors = bookDao.findAllAuthors();

        assertEquals(2, allAuthors.size());

    }
}