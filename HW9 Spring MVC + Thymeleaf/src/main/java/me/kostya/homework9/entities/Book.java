package me.kostya.homework9.entities;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.*;
import java.util.stream.Collectors;

@Document
public class Book {

    @Id
    private String id;
    private String name;

    private Genre genre;

    private Set<Author> authors;

    private List<Comment> comments;

    public static Book create(String name) {
        return create(null, name, null);
    }

    public static Book create(String name, String genreName) {
        return create(null, name, Genre.create(genreName));
    }

    public static Book create(String id, String name, Genre genre) {
        Book book = new Book();
        book.id = id;
        book.setName(name);
        book.setGenre(genre);
        book.authors = new HashSet<>();
        book.comments = new ArrayList<>();
        return book;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Set<Author> getAuthors() {
        return Collections.unmodifiableSet(authors);
    }

    public String getAuthorsAsString() {
        return authors.stream().map(Author::getName).collect(Collectors.joining(", "));
    }


    public void addAuthor(Author author) {
        if(author != null) {
            authors.add(author);
        }
    }

    public void addAuthors(Collection<Author> authors) {
        if(authors != null) {
            this.authors.addAll(authors);
        }
    }

    public void deleteAuthor(Author author) {
        if(author != null) {
            authors.remove(author);
        }
    }

    public void deleteAuthors() {
        authors.clear();
    }

    public List<Comment> getComments() {
        return Collections.unmodifiableList(comments);
    }

    public void addComment(Comment comment) {
        if(comment != null) {
            comments.add(comment);
        }
    }

    public void deleteComments() {
        comments.clear();
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", genre=" + genre +
                ", authors=" + authors +
                ", comments=" + comments +
                '}';
    }
}
