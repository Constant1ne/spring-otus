package me.kostya.homework9.entities;


public class Comment {

    private String text;

    private Comment() {
    }

    public static Comment create(String text) {
        Comment comment = new Comment();
        comment.text = text;
        return  comment;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "Comment{" +
                " text='" + text + '\'' +
                '}';
    }
}
