package me.kostya.homework9.dao;

import me.kostya.homework9.entities.Author;
import me.kostya.homework9.entities.Genre;

import java.util.List;

public interface CustomBookDao {
    List<Genre> findAllGenres();
    List<Author> findAllAuthors();
}
