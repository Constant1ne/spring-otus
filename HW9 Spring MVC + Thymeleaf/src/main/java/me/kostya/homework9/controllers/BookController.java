package me.kostya.homework9.controllers;

import me.kostya.homework9.entities.Author;
import me.kostya.homework9.entities.Book;
import me.kostya.homework9.entities.Genre;
import me.kostya.homework9.servicies.BookshelfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class BookController {
    private final BookshelfService bookshelfService;

    public BookController(@Autowired BookshelfService bookshelfService) {
        this.bookshelfService = bookshelfService;
    }

    @GetMapping("/")
    public String list(Model model) {
        List<Book> allBooks = bookshelfService.getAllBooks();
        model.addAttribute("books", allBooks);

        return "bookList";
    }

    @GetMapping("/create")
    public String create() {
        return "bookCreate";
    }

    @GetMapping("/update")
    public String update(@RequestParam(value = "id") String id,
                       Model model) {
        Book book = bookshelfService.getBook(id);
        model.addAttribute("book", book);

        return "bookUpdate";
    }

    @PostMapping("/update")
    public String update(@RequestParam(value = "id", required = false) String id,
                         @RequestParam("name") String name,
                         @RequestParam("genre") String genre,
                         @RequestParam("authors") String authors) {

        Book book = Book.create(id, name, Genre.create(genre));

        Set<Author> bookAuthors = Arrays.stream(authors.split(","))
                .map(String::trim)
                .map(Author::create)
                .collect(Collectors.toSet());
        book.addAuthors(bookAuthors);

        bookshelfService.saveBook(book);

        return "redirect:/";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam(value = "id") String id) {
        bookshelfService.deleteBook(id);

        return "redirect:/";
    }
}
