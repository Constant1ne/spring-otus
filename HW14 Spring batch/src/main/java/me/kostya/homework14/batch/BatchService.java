package me.kostya.homework14.batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BatchService {
    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    Job job;

    public void restart() throws Exception{
        jobLauncher.run(job, new JobParameters());
    }
}
