package me.kostya.homework14.batch;

import me.kostya.homework14.sql.entity.Book;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.MongoItemReader;
import org.springframework.batch.item.data.MongoItemWriter;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;

import javax.persistence.EntityManagerFactory;
import java.util.*;
import java.util.stream.Collectors;

@Configuration
public class MyBatchConfig {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    private MongoOperations mongoOperations;


    @Bean
    public ExecutionContext executionContext() {
        return new ExecutionContext();
    }


    @Bean
    public CompositeItemWriter<me.kostya.homework14.nosql.entity.Book> compositeItemWriter() {
        CompositeItemWriter<me.kostya.homework14.nosql.entity.Book> writer = new CompositeItemWriter<>();
        writer.setDelegates(Arrays.asList(badSoftBadWriter(), mongoItemWriter()));
        return writer;
    }

    @Bean
    public ItemWriter<me.kostya.homework14.nosql.entity.Book> badSoftBadWriter() {
        return new ItemWriter<me.kostya.homework14.nosql.entity.Book>() {
            JpaItemWriter<Book> jpaWriter = new JpaItemWriter<>();
            {
                jpaWriter.setEntityManagerFactory(entityManagerFactory);
            }

            @Override
            public void write(List<? extends me.kostya.homework14.nosql.entity.Book> items) throws Exception {
                jpaWriter.write(items.stream().map(book -> Book.create(book.getName())).collect(Collectors.toList()));
            }
        };
    }



    @Bean
    public MongoItemWriter<me.kostya.homework14.nosql.entity.Book> mongoItemWriter() {
        MongoItemWriter<me.kostya.homework14.nosql.entity.Book> writer = new MongoItemWriter<>();
        writer.setTemplate(mongoOperations);
        writer.setCollection("book");
        return writer;
    }

    @Bean
    public ItemProcessor<me.kostya.homework14.nosql.entity.Book, me.kostya.homework14.nosql.entity.Book> processor() {
        return me.kostya.homework14.nosql.entity.Book::migrated;
    }

    @Bean
    @StepScope
    public MongoItemReader<me.kostya.homework14.nosql.entity.Book> reader() {
        MongoItemReader<me.kostya.homework14.nosql.entity.Book> mongoItemReader = new MongoItemReader<>();

        //String count = mongoItemReader.getExecutionContextKey("MongoItemReader.read.count");

        mongoItemReader.setTemplate(mongoOperations);
        mongoItemReader.setTargetType(me.kostya.homework14.nosql.entity.Book.class);
        mongoItemReader.setCollection("book");
        mongoItemReader.setQuery("{migratedToPostgres: {$not : {$eq: true}}}");

        Map<String, Sort.Direction> sort = new HashMap<String, Sort.Direction>();
        sort.put("_id", Sort.Direction.ASC);
        mongoItemReader.setSort(sort);

        return mongoItemReader;
    }

    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1")
                .<me.kostya.homework14.nosql.entity.Book, me.kostya.homework14.nosql.entity.Book>chunk(10)
                .reader(reader())
                .processor(processor())
                .writer(compositeItemWriter())
                .allowStartIfComplete(true)
                .build();
    }

    @Bean
    public Job copyBooksFromMongoToPostgres() {
        return jobBuilderFactory.get("copyBooksFromMongoToPostgres")
                .start(step1())
                .build();
    }

}