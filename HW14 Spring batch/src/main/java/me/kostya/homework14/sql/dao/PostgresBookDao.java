package me.kostya.homework14.sql.dao;

import me.kostya.homework14.sql.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostgresBookDao extends JpaRepository<Book, Long> {
}