package me.kostya.homework14.nosql.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Book {

    @Id()
    private String id;

    private String name;

    private boolean migratedToPostgres;

    public boolean isMigratedToPostgres() {
        return migratedToPostgres;
    }

    public Book migrated() {
        this.migratedToPostgres = true;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Book create(String name) {
        Book book = new Book();
        book.name = name;
        return book;
    }
}
