package me.kostya.homework14.nosql.dao;

import me.kostya.homework14.nosql.entity.Book;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface MongoBookDao extends MongoRepository<Book, String> {
}