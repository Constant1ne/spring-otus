package me.kostya.homework14.shell;

import me.kostya.homework14.batch.BatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

@ShellComponent
public class ShellCommands {

    @Autowired
    BatchService batchService;

    @ShellMethod("Restart migration")
    public void restart() {
        try {
            batchService.restart();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
