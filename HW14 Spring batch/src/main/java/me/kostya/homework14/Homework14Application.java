package me.kostya.homework14;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@EnableBatchProcessing
public class Homework14Application {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(Homework14Application.class, args);

		System.out.println("zzzzzzzzzzzzzzzzzzzzzzzzzzz");
	}
}
