DROP SCHEMA IF EXISTS BOOKSHELF cascade;

create schema BOOKSHELF;

SET SCHEMA BOOKSHELF;

CREATE TABLE authors
(
    id bigint AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name text NOT NULL
);


create table BOOKS
(
	id bigint AUTO_INCREMENT PRIMARY KEY NOT NULL,
	NAME CLOB not null,
	GENREID BIGINT
)
;

create table AUTHORSHIP
(
	BOOKID BIGINT not null,
	AUTHORID BIGINT not null,
	constraint AUTHORSHIP_PK
		primary key (BOOKID, AUTHORID),
	constraint AUTHORSHIP_AUTHORS_ID_FK
		foreign key (AUTHORID) references AUTHORS,
	constraint AUTHORSHIP_BOOKS_ID_FK
		foreign key (BOOKID) references BOOKS
)
;

create table GENRES
(
	id bigint AUTO_INCREMENT PRIMARY KEY NOT NULL,
	NAME CLOB not null
)
;