package me.kostya.homework5.servicies;

import me.kostya.homework5.entities.Author;
import me.kostya.homework5.entities.Book;
import me.kostya.homework5.entities.Genre;

import java.util.Collection;
import java.util.List;

public interface BookshelfService {
    long addAuthor(Author author);
    long addGenre(Genre genre);
    long addBook(Book book);

    List<Author> getAllAuthors();
    List<Book> getAllBooks();
    List<Genre> getAllGenres();

    Book getBook(long id);
    Author getAuthor(long id);

    void updateBook(Book book);
    void updateAuthor(Author author);

    void deleteAuthor(long id);
    void deleteBook(long id);

    List<Long> getBookAuthors(long bookId);
    void updateBookAuthors(long bookId, Collection<Long> authorIds);
}
