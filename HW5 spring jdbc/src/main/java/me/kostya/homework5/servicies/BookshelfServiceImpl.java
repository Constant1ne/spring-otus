package me.kostya.homework5.servicies;

import me.kostya.homework5.dao.AuthorDao;
import me.kostya.homework5.dao.AuthorshipDao;
import me.kostya.homework5.dao.BookDao;
import me.kostya.homework5.dao.GenreDao;
import me.kostya.homework5.entities.Author;
import me.kostya.homework5.entities.Book;
import me.kostya.homework5.entities.Genre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class BookshelfServiceImpl implements BookshelfService{

    private BookDao bookDao;
    private AuthorDao authorDao;
    private GenreDao genreDao;
    private AuthorshipDao authorshipDao;

    public BookshelfServiceImpl(@Autowired BookDao bookDao,
                                @Autowired AuthorDao authorDao,
                                @Autowired GenreDao genreDao,
                                @Autowired AuthorshipDao authorshipDao) {
        this.bookDao = bookDao;
        this.authorDao = authorDao;
        this.genreDao = genreDao;
        this.authorshipDao = authorshipDao;
    }

    @Override
    public long addAuthor(Author author) {
        return authorDao.insert(author);
    }

    @Override
    public long addGenre(Genre genre) {
        return genreDao.insert(genre);
    }

    @Override
    public long addBook(Book book) {
        book.setGenre(getOrCreateGenre(book.getGenre()));
        return bookDao.insert(book);
    }

    @Override
    public List<Author> getAllAuthors() {
        return authorDao.findAll();
    }

    @Override
    public List<Book> getAllBooks() {
        return bookDao.findAll();
    }

    @Override
    public List<Genre> getAllGenres() {
        return genreDao.findAll();
    }

    @Override
    public Book getBook(long id) {
        return bookDao.findById(id);
    }

    @Override
    public Author getAuthor(long id) {
        return authorDao.findById(id);
    }

    @Override
    public void updateBook(Book book) {
        book.setGenre(getOrCreateGenre(book.getGenre()));
        bookDao.update(book);
    }

    @Override
    public void updateAuthor(Author author) {
        authorDao.update(author);
    }

    @Override
    public void deleteAuthor(long id) {
        authorshipDao.deleteAuthorBooks(id);
        authorDao.deleteById(id);
    }

    @Override
    public void deleteBook(long id) {
        authorshipDao.deleteBookAuthors(id);
        bookDao.deleteById(id);
    }

    @Override
    public List<Long> getBookAuthors(long bookId) {
        return authorshipDao.getBookAuthorsIds(bookId);
    }

    @Override
    public void updateBookAuthors(long bookId, Collection<Long> authorIds) {
        authorshipDao.deleteBookAuthors(bookId);
        //начинаю лениться
        authorshipDao.addBookAuthors(bookId, authorIds);
    }

    private Genre getOrCreateGenre(Genre genre) {
        if(genre != null) {
            try {
                return genreDao.findById(genre.getId());
            } catch (Exception ex) {
                List<Genre> genres = genreDao.findByName(genre.getName());
                if(genres.isEmpty()) {
                    long id = genreDao.insert(genre);
                    genre.setId(id);
                    return genre;
                } else {
                    return genres.get(0);
                }
            }
        } else {
            return genreDao.getDefaultGenre();
        }
    }
}
