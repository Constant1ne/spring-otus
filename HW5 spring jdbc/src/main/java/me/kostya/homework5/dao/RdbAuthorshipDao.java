package me.kostya.homework5.dao;

import me.kostya.homework5.entities.Author;
import me.kostya.homework5.entities.Book;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Repository
public class RdbAuthorshipDao implements AuthorshipDao {

    private final NamedParameterJdbcOperations jdbc;

    public RdbAuthorshipDao(NamedParameterJdbcOperations jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public List<Long> getBookAuthorsIds(long bookId) {
        return jdbc.queryForList("SELECT authorId FROM authorship WHERE bookid = :bookid",
                Collections.singletonMap("bookid", bookId),
                Long.class);
    }

    @Override
    public void addBookAuthors(long bookId, Collection<Long> authorIds) {
        SqlParameterSource [] batchParams = authorIds.stream()
                .map(authorId -> new MapSqlParameterSource()
                        .addValue("bookid", bookId)
                        .addValue("authorid", authorId))
                .toArray(SqlParameterSource[]::new);

        jdbc.batchUpdate("INSERT INTO authorship (bookid, authorid) values (:bookid, :authorid)", batchParams);
    }

    @Override
    public List<Long> getAuthorBooksIds(Author author) {
        return jdbc.queryForList("SELECT bookId FROM authorship WHERE authorid = :authorid",
                Collections.singletonMap("authorid", author.getId()),
                Long.class);
    }

    @Override
    public int deleteAuthorship(Book book, Author author) {
        throw new UnsupportedOperationException("TODO");
    }

    @Override
    public int deleteBookAuthors(long bookId) {
        return jdbc.update("DELETE FROM authorship WHERE bookid = :bookid", Collections.singletonMap("bookid", bookId));
    }

    @Override
    public int deleteAuthorBooks(long authorId) {
        return jdbc.update("DELETE FROM authorship WHERE authorid = :authorid", Collections.singletonMap("authorid", authorId));
    }
}
