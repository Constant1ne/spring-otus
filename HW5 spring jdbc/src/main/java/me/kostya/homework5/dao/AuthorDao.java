package me.kostya.homework5.dao;

import me.kostya.homework5.entities.Author;

import java.util.Collection;
import java.util.List;

public interface AuthorDao {

    Author findById(long id);

    List<Author> findByName(String name);

    List<Author> findAll();

    long count();

    long insert(Author author);

    int update(Author author);

    int deleteById(long id);

}
