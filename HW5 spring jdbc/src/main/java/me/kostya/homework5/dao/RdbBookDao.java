package me.kostya.homework5.dao;

import me.kostya.homework5.entities.Book;
import me.kostya.homework5.entities.Genre;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Repository
public class RdbBookDao implements BookDao {

    private NamedParameterJdbcOperations jdbc;

    private final RowMapper<Book> rowMapper = (rs, rowNum) -> {
        Book book = new Book();
        book.setId(rs.getLong("ID"));
        book.setName(rs.getString("NAME"));

        book.setGenre(Genre.create(rs.getLong("genreId"), rs.getString("genreName")));

        return book;
    };

    public RdbBookDao(NamedParameterJdbcOperations jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public Book findById(long id) {
        return jdbc.queryForObject("SELECT b.id, b.name, b.genreid, g.name as genreName FROM books b " +
                        "LEFT JOIN GENRES g ON b.genreid = g.id" +
                        " WHERE b.id=:id;",
                Collections.singletonMap("id", id),
                rowMapper);
    }

    @Override
    public List<Book> findByName(String name) {
        return jdbc.query("SELECT b.id, b.name, b.genreid, g.name as genreName FROM books b " +
                        "LEFT JOIN GENRES g ON b.genreid = g.id" +
                        " WHERE b.name=:name;",
                Collections.singletonMap("name", name),
                rowMapper);
    }

    @Override
    public List<Book> findAll() {
        return jdbc.query("SELECT b.id, b.name, b.genreid, g.name as genreName FROM books b " +
                        "LEFT JOIN GENRES g ON b.genreid = g.id",
                rowMapper);
    }

    @Override
    public long count() {
        return jdbc.queryForObject("SELECT COUNT(*) FROM books;",
                Collections.emptyMap(),
                Integer.class);
    }

    @Override
    public long insert(Book book) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue( "name", book.getName())
                .addValue("genreId", book.getGenre().getId());

        final KeyHolder holder = new GeneratedKeyHolder();

        jdbc.update( "INSERT INTO books (name, genreId) values (:name, :genreId)", params, holder, new String[] {"ID"} );
        Number generatedId = holder.getKey();

        return generatedId.longValue();
    }

    @Override
    public int update(Book book) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", book.getId())
                .addValue("name", book.getName())
                .addValue("genreId", book.getGenre().getId());

        return jdbc.update("UPDATE books SET name = :name, genreId = :genreId WHERE id=:id", params);
    }

    @Override
    public int deleteById(long id) {
        return jdbc.update("DELETE FROM books WHERE id = :id", Collections.singletonMap("id", id));
    }
}
