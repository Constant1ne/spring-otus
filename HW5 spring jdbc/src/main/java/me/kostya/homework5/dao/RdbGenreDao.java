package me.kostya.homework5.dao;

import me.kostya.homework5.entities.Genre;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
public class RdbGenreDao implements GenreDao {

    private final NamedParameterJdbcOperations jdbc;

    static final RowMapper<Genre> rowMapper = (rs, rowNum) -> {
        Genre genre = new Genre();
        genre.setId(rs.getLong("ID"));
        genre.setName(rs.getString("NAME"));
        return genre;
    };

    public RdbGenreDao(NamedParameterJdbcOperations jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public Genre findById(long id) {
        return jdbc.queryForObject("SELECT * FROM genres WHERE id=:id;",
                Collections.singletonMap("id", id),
                rowMapper);
    }

    @Override
    public List<Genre> findByName(String name) {
        return jdbc.query("SELECT * FROM genres WHERE name=:name;",
                Collections.singletonMap("name", name),
                rowMapper);
    }

    @Override
    public List<Genre> findAll() {
        return jdbc.query("SELECT * FROM genres",rowMapper);
    }

    @Override
    public long count() {
        return jdbc.queryForObject("SELECT COUNT(*) FROM genres;",
                Collections.emptyMap(),
                Integer.class);
    }

    @Override
    public long insert(Genre genre) {
        SqlParameterSource parameters = new MapSqlParameterSource().
                addValue( "name", genre.getName());

        final KeyHolder holder = new GeneratedKeyHolder();

        jdbc.update( "INSERT INTO genres (name) values (:name)", parameters, holder, new String[] {"ID"} );
        Number generatedId = holder.getKey();

        return generatedId.longValue();
    }

    @Override
    public int update(Genre genre) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("name", genre.getName())
                .addValue("id", genre.getId());

        return jdbc.update("UPDATE genres SET name = :name WHERE id=:id", params);
    }

    @Override
    public int deleteById(long id) {
        return jdbc.update("DELETE FROM genres WHERE id = :id", Collections.singletonMap("id", id));
    }

    @Override
    public Genre getDefaultGenre() {
        return findById(1L);
    }
}
