package me.kostya.homework5.dao;

import me.kostya.homework5.entities.Author;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class RdbAuthorDao implements AuthorDao {

    private final NamedParameterJdbcOperations jdbc;

    private final RowMapper<Author> rowMapper = (rs, rowNum) -> {
        Author author = new Author();
        author.setId(rs.getLong("ID"));
        author.setName(rs.getString("NAME"));
        return author;
    };

    public RdbAuthorDao(NamedParameterJdbcOperations jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public Author findById(long id) {
        return jdbc.queryForObject("SELECT * FROM authors WHERE id=:id;",
                Collections.singletonMap("id", id),
                rowMapper);
    }

    @Override
    public List<Author> findByName(String name) {
        return jdbc.query("SELECT * FROM authors WHERE name=:name;",
                Collections.singletonMap("name", name),
                rowMapper);
    }

    @Override
    public List<Author> findAll() {
        return jdbc.query("SELECT * FROM authors",rowMapper);
    }

    @Override
    public long count() {
        return jdbc.queryForObject("SELECT COUNT(*) FROM authors;",
                Collections.emptyMap(),
                Integer.class);
    }

    @Override
    public long insert(Author author) {
        SqlParameterSource parameters = new MapSqlParameterSource().
                addValue( "name", author.getName());

        final KeyHolder holder = new GeneratedKeyHolder();

        jdbc.update( "INSERT INTO authors (name) values (:name)", parameters, holder, new String[] {"ID"} );
        Number generatedId = holder.getKey();

        return generatedId.longValue();
    }

    @Override
    public int update(Author author) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("name", author.getName())
                .addValue("id", author.getId());

        return jdbc.update("UPDATE authors SET name = :name WHERE id=:id", params);
    }

    @Override
    public int deleteById(long id) {
        return jdbc.update("DELETE FROM authors WHERE id = :id", Collections.singletonMap("id", id));
    }
}
