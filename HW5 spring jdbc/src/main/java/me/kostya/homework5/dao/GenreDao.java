package me.kostya.homework5.dao;

import me.kostya.homework5.entities.Genre;

import java.util.List;

public interface GenreDao {

    Genre findById(long id);

    List<Genre> findByName(String name);

    List<Genre> findAll();

    long count();

    long insert(Genre genre);

    int update(Genre genre);

    int deleteById(long id);

    Genre getDefaultGenre();

}
