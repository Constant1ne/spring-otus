package me.kostya.homework5.dao;

import me.kostya.homework5.entities.Book;

import java.util.List;

public interface BookDao {

    Book findById(long id);

    List<Book> findByName(String name);

    List<Book> findAll();

    long count();

    long insert(Book author);

    int update(Book author);

    int deleteById(long id);

}
