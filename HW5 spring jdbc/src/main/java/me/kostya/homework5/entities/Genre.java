package me.kostya.homework5.entities;

public class Genre {
    private long id;
    private String name;

    public static Genre create(long id) {
        return create(id, null);
    }

    public static Genre create(String name) {
        return create(0, name);
    }

    public static Genre create(long id, String name) {
        Genre genre = new Genre();
        genre.setId(id);
        genre.setName(name);
        return genre;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
