package me.kostya.homework5.entities;

import java.util.Collection;

public class Book {
    private long id;
    private String name;
    private Genre genre;

    public static Book create(long id) {
        return create(id, null, null);
    }

    public static Book create(String name) {
        return create(0, name, null);
    }

    public static Book create(String name, String genreName) {
        return create(0, name, Genre.create(genreName));
    }

    public static Book create(long id, String name, Genre genre) {
        Book book = new Book();
        book.setId(id);
        book.setName(name);
        book.setGenre(genre);
        return book;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", genre=" + genre +
                '}';
    }
}
