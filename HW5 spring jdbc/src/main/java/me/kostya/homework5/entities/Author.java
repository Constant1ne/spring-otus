package me.kostya.homework5.entities;

public class Author {
    private long id;
    private String name;

    public static Author create(long id) {
        return create(id, null);
    }

    public static Author create(String name) {
        return create(0, name);
    }

    public static Author create(long id, String name) {
        Author author = new Author();
        author.setId(id);
        author.setName(name);
        return author;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Author{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
