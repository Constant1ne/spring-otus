package me.kostya.homework5.view;

import me.kostya.homework5.entities.Author;
import me.kostya.homework5.entities.Book;
import me.kostya.homework5.entities.Genre;
import me.kostya.homework5.servicies.BookshelfService;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@ShellComponent
public class ShellView {

    private BookshelfService bookshelf;

    public ShellView(BookshelfService bookshelf) {
        this.bookshelf = bookshelf;
    }

    @ShellMethod("View all authors")
    public String listAuthors() {
        StringBuilder sb = new StringBuilder();

        bookshelf.getAllAuthors().stream()
                .forEach(a -> sb.append(a.getId())
                        .append(" ")
                        .append(a.getName())
                        .append("\r\n"));

        return sb.toString();
    }

    @ShellMethod("View all genres")
    public String listGenres() {
        StringBuilder sb = new StringBuilder();

        bookshelf.getAllGenres().stream()
                .forEach(a -> sb.append(a.getId())
                        .append(" ")
                        .append(a.getName())
                        .append("\r\n"));

        return sb.toString();
    }

    @ShellMethod("View all books")
    public String listBooks() {
        StringBuilder sb = new StringBuilder();

        bookshelf.getAllBooks().stream()
                .forEach(book -> sb.append(book.getId())
                        .append(" ")
                        .append(book.getName())
                        .append(" ")
                        .append(book.getGenre().getName())
                        .append("\r\n"));

        return sb.toString();
    }

    @ShellMethod("Add author")
    public String addAuthor(String authorName) {
        long id = bookshelf.addAuthor(Author.create(authorName));
        return "Author id is " + id;
    }

    @ShellMethod("Add genre")
    public String addGenre(String genreName) {
        long id = bookshelf.addGenre(Genre.create(genreName));
        return "Genre id is " + id;
    }

    @ShellMethod("Add Book")
    public String addBook(String bookName, String genreName) {
        long id = bookshelf.addBook(Book.create(bookName, genreName));
        return "Book id is " + id;
    }

    @ShellMethod("Update Book")
    public String updateBook(long bookId, String bookName, String genreName) {
        try {
            bookshelf.updateBook(Book.create(bookId, bookName, Genre.create(genreName)));
            return "OK";
        } catch (Exception ex) {
            return "Error";
        }
    }

    @ShellMethod("Update Author")
    public String updateAuthor(long authorId, String authorName) {
        try {
            bookshelf.updateAuthor(Author.create(authorId, authorName));
            return "OK";
        } catch (Exception ex) {
            return "Error";
        }
    }

    @ShellMethod("List Book Authors")
    public String listBookAuthors(long bookId) {
        StringBuilder sb = new StringBuilder();
        bookshelf.getBookAuthors(bookId).stream()
                .map(bookshelf::getAuthor)
                .forEach(a -> sb.append(a.getId())
                        .append(" ")
                        .append(a.getName())
                        .append("\r\n"));

        return sb.toString();
    }

    @ShellMethod("Set Book Authors")
    public String setBookAuthors(long bookId, Long ... authorIds) {
        bookshelf.updateBookAuthors(bookId, Arrays.asList(authorIds));
        return "OK";
    }

    @ShellMethod("Delete Book")
    public String deleteBook(long bookId) {
        try {
            bookshelf.deleteBook(bookId);
            return "OK";
        } catch (Exception ex) {
            return "Error";
        }
    }
}
