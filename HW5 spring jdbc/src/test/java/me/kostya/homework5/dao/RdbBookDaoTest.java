package me.kostya.homework5.dao;

import me.kostya.homework5.entities.Book;
import me.kostya.homework5.entities.Genre;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.shell.jline.InteractiveShellApplicationRunner;
import org.springframework.shell.jline.ScriptShellApplicationRunner;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = {
        InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED + "=false",
        ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT_ENABLED + "=false"
})
class RdbBookDaoTest {

    @Autowired
    private RdbBookDao bookDao;

    @Test
    void findById() {
        assertEquals("War and Peace", bookDao.findById(1L).getName());
    }

    @Test
    void findByName() {
        assertEquals(1, bookDao.findByName("War and Peace").get(0).getId());
    }

    @Test
    void findAll() {
        assertEquals(4, bookDao.findAll().size());
    }

    @Test
    void count() {
        assertEquals(4, bookDao.count());
    }

    @Test
    @DirtiesContext
    void insert() {
        Book book = Book.create(0, "John Gold", Genre.create(1));

        long id = bookDao.insert(book);

        assertEquals(book.getName(), bookDao.findById(id).getName());
    }

    @Test
    @DirtiesContext
    void update() {
        Book book = bookDao.findById(1);

        book.setName("John Gold");

        bookDao.update(book);

        Book result = bookDao.findById(1);

        assertEquals("John Gold", result.getName());
    }

    @Test
    @DirtiesContext
    void deleteById() {
        Book book = Book.create(0, "John Gold", Genre.create(1));
        long id = bookDao.insert(book);
        assertEquals(book.getName(), bookDao.findById(id).getName());

        bookDao.deleteById(id);

        assertThrows(EmptyResultDataAccessException.class, () -> bookDao.findById(id));
    }
}