package me.kostya.homework5.dao;

import me.kostya.homework5.entities.Author;
import me.kostya.homework5.entities.Book;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.shell.jline.InteractiveShellApplicationRunner;
import org.springframework.shell.jline.ScriptShellApplicationRunner;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = {
        InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED + "=false",
        ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT_ENABLED + "=false"
})
class RdbAuthorshipDaoTest {

    @Autowired
    private RdbAuthorshipDao authorshipDao;

    @Test
    void getBookAuthorsIds() {
        List<Long> ids = authorshipDao.getBookAuthorsIds(4);
        assertEquals(3, ids.size());
    }

    @Test
    @DirtiesContext
    void addBookAuthors() {
        authorshipDao.addBookAuthors(1, Collections.singletonList(1L));

        List<Long> ids = authorshipDao.getBookAuthorsIds(1);

        assertEquals(2, ids.size());
    }

    @Test
    void getAuthorBooksIds() {
        List<Long> ids = authorshipDao.getAuthorBooksIds(Author.create(1));
        assertEquals(2, ids.size());
    }

    @Test
    @DirtiesContext
    void deleteAuthorship() {
    }

    @Test
    @DirtiesContext
    void deleteBookAuthors() {
        List<Long> ids = authorshipDao.getBookAuthorsIds(4);
        assertTrue(!ids.isEmpty());

        authorshipDao.deleteBookAuthors(4);

        ids = authorshipDao.getBookAuthorsIds(4);
        assertTrue(ids.isEmpty());
    }

    @Test
    @DirtiesContext
    void deleteAuthorBooks() {
        Author author = Author.create(1);

        List<Long> ids = authorshipDao.getAuthorBooksIds(author);
        assertTrue(!ids.isEmpty());

        authorshipDao.deleteAuthorBooks(author.getId());

        ids = authorshipDao.getAuthorBooksIds(author);
        assertTrue(ids.isEmpty());
    }
}