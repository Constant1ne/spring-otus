package me.kostya.homework5.dao;

import me.kostya.homework5.entities.Author;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.shell.jline.InteractiveShellApplicationRunner;
import org.springframework.shell.jline.ScriptShellApplicationRunner;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = {
        InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED + "=false",
        ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT_ENABLED + "=false"
})
class RdbAuthorDaoTest {

    @Autowired
    private RdbAuthorDao authorDao;

    @Test
    void findById() {
        assertEquals("Dostoevskiy",authorDao.findById(1L).getName());
    }

    @Test
    void findByName() {
        assertEquals(1,authorDao.findByName("Dostoevskiy").get(0).getId());
    }

    @Test
    void findAll() {
        assertEquals(4, authorDao.findAll().size());
    }

    @Test
    void count() {
        assertEquals(4, authorDao.count());
    }

    @Test
    @DirtiesContext
    void insert() {
        Author author = new Author();
        author.setName("Gogol");

        long id = authorDao.insert(author);

        assertEquals(author.getName(), authorDao.findById(id).getName());
    }

    @Test
    @DirtiesContext
    void update() {
        Author author = authorDao.findById(1);

        author.setName("Vasya");


        authorDao.update(author);

        Author result = authorDao.findById(1);

        assertEquals("Vasya", result.getName());
    }

    @Test
    @DirtiesContext
    void deleteById() {
        List<Author> authors = authorDao.findByName("Turgeniev");
        assertTrue(!authors.isEmpty());

        int status = authorDao.deleteById(authors.get(0).getId());

        assertTrue(authorDao.findByName("Turgeniev").isEmpty());
    }
}