package me.kostya.homework5.dao;

import me.kostya.homework5.entities.Genre;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.shell.jline.InteractiveShellApplicationRunner;
import org.springframework.shell.jline.ScriptShellApplicationRunner;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = {
        InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED + "=false",
        ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT_ENABLED + "=false"
})
class RdbGenreDaoTest {

    @Autowired
    private RdbGenreDao genreDao;

    @Test
    void findById() {
        assertEquals("novel", genreDao.findById(1L).getName());
    }

    @Test
    void findByName() {
        assertEquals(1, genreDao.findByName("novel").get(0).getId());
    }

    @Test
    void findAll() {
        assertEquals(3, genreDao.findAll().size());
    }

    @Test
    void count() {
        assertEquals(3, genreDao.count());
    }

    @Test
    @DirtiesContext
    void insert() {
        Genre genre = new Genre();
        genre.setName("Song");

        long id = genreDao.insert(genre);

        assertEquals(genre.getName(), genreDao.findById(id).getName());
    }

    @Test
    @DirtiesContext
    void update() {
        Genre genre = genreDao.findById(1);

        genre.setName("Song");


        genreDao.update(genre);

        Genre result = genreDao.findById(1);

        assertEquals("Song", result.getName());
    }

    @Test
    @DirtiesContext
    void deleteById() {
        List<Genre> genres = genreDao.findByName("satire");
        assertTrue(!genres.isEmpty());

        int status = genreDao.deleteById(genres.get(0).getId());

        assertTrue(genreDao.findByName("satire").isEmpty());
    }
}