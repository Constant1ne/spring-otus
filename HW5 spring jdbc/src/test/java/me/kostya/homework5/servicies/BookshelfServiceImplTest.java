package me.kostya.homework5.servicies;

import me.kostya.homework5.entities.Author;
import me.kostya.homework5.entities.Book;
import me.kostya.homework5.entities.Genre;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.shell.jline.InteractiveShellApplicationRunner;
import org.springframework.shell.jline.ScriptShellApplicationRunner;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = {
        InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED + "=false",
        ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT_ENABLED + "=false"
})
class BookshelfServiceImplTest {

    @Autowired
    BookshelfService bookshelf;

    @Test
    @DirtiesContext
    void addAuthor() {
        Author author = new Author();
        author.setName("Gogol");

        long id = bookshelf.addAuthor(author);

        assertEquals(author.getName(), bookshelf.getAuthor(id).getName());
    }

    @Test
    @DirtiesContext
    void addGenre() {
        Genre genre = new Genre();
        genre.setName("Gogol");

        long id = bookshelf.addGenre(genre);

        assertTrue(id > 0);
    }

    @Test
    @DirtiesContext
    void addBook() {
        Book book = Book.create("John Gold");

        long id = bookshelf.addBook(book);

        assertEquals(book.getName(), bookshelf.getBook(id).getName());
    }

    @Test
    void getAllAuthors() {
        assertEquals(4, bookshelf.getAllAuthors().size());
    }

    @Test
    void getAllBooks() {
        assertEquals(4, bookshelf.getAllBooks().size());
    }

    @Test
    void getAllGenres() {
        assertEquals(3, bookshelf.getAllGenres().size());
    }

    @Test
    void getBook() {
        assertEquals("War and Peace", bookshelf.getBook(1L).getName());
    }

    @Test
    void getAuthor() {
        assertEquals("Dostoevskiy",bookshelf.getAuthor(1L).getName());
    }

    @Test
    @DirtiesContext
    void updateBook() {
        Book book = bookshelf.getBook(1);

        book.setName("John Gold");
        book.setGenre(Genre.create(3));

        bookshelf.updateBook(book);

        Book result = bookshelf.getBook(1);

        assertEquals("John Gold", result.getName());
        assertEquals(3, result.getGenre().getId());
    }

    @Test
    @DirtiesContext
    void updateAuthor() {
        Author author = bookshelf.getAuthor(1);

        author.setName("Vasya");

        bookshelf.updateAuthor(author);

        Author result = bookshelf.getAuthor(1);

        assertEquals("Vasya", result.getName());
    }

    @Test
    @DirtiesContext
    void deleteAuthor() {
        bookshelf.getAuthor(4);

        bookshelf.deleteAuthor(4);

        assertThrows(EmptyResultDataAccessException.class, () -> bookshelf.getAuthor(4));
    }

    @Test
    @DirtiesContext
    void deleteBook() {
        bookshelf.getBook(4);

        bookshelf.deleteBook(4);

        assertThrows(EmptyResultDataAccessException.class, () -> bookshelf.getBook(4));
    }

    @Test
    @DirtiesContext
    void updateBookAuthors() {
        assertEquals(3, bookshelf.getBookAuthors(4).size());

        bookshelf.updateBookAuthors(4, Collections.singletonList(1L));

        assertEquals(1, bookshelf.getBookAuthors(4).size());
    }
}