package me.kostya.homework7.servicies;

import me.kostya.homework7.entities.Author;
import me.kostya.homework7.entities.Book;
import me.kostya.homework7.entities.Comment;
import me.kostya.homework7.entities.Genre;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.shell.jline.InteractiveShellApplicationRunner;
import org.springframework.shell.jline.ScriptShellApplicationRunner;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = {
        InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED + "=false",
        ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT_ENABLED + "=false"
})
class BookshelfServiceImplTest {

    @Autowired
    BookshelfService bookshelf;

    @BeforeEach
    void setUp() {
        bookshelf.deleteAll();
    }

    @Test
    void addAuthor() {
        Author author = new Author();
        author.setName("Gogol");

        bookshelf.addAuthor(author);

        assertEquals(author.getName(), bookshelf.getAuthor(author.getId()).getName());
    }

    @Test
    void addGenre() {
        Genre genre = new Genre();
        genre.setName("Novel");

        bookshelf.addGenre(genre);

        assertTrue(genre.getId() > 0);
    }

    @Test
    void addBook() {
        Book book = Book.create("John Gold");

        bookshelf.addBook(book);

        assertEquals(book.getName(), bookshelf.getBook(book.getId()).getName());
    }

    @Test
    void getAllAuthors() {
        bookshelf.addAuthor(Author.create("Vasya"));
        bookshelf.addAuthor(Author.create("Ivan"));
        assertEquals(2, bookshelf.getAllAuthors().size());
    }

    @Test
    void getAllBooks() {
        bookshelf.addBook(Book.create("abc"));
        bookshelf.addBook(Book.create("qwe"));
        assertEquals(2, bookshelf.getAllBooks().size());
    }

    @Test
    void getAllGenres() {
        bookshelf.addGenre(Genre.create("zxc"));
        bookshelf.addGenre(Genre.create("fgh"));
        assertEquals(2, bookshelf.getAllGenres().size());
    }

    @Test
    void getBook() {
        Book book = Book.create("abc");
        bookshelf.addBook(book);
        assertEquals("abc", bookshelf.getBook(book.getId()).getName());
    }

    @Test
    void getAuthor() {
        Author author = Author.create("abc");
        bookshelf.addAuthor(author);
        assertEquals("abc", bookshelf.getAuthor(author.getId()).getName());
    }

    @Test
    void updateBook() {
        Book book = Book.create("abc");
        bookshelf.addBook(book);

        Book result = bookshelf.getBook(book.getId());

        result.setName("John Gold");
        result.setGenre(Genre.create("asd"));

        bookshelf.updateBook(result);

        Book check = bookshelf.getBook(result.getId());

        assertEquals("John Gold", check.getName());
        assertEquals("asd", check.getGenre().getName());
    }

    @Test
    void updateAuthor() {
        Author author = Author.create("abc");
        bookshelf.addAuthor(author);

        Author found = bookshelf.getAuthor(author.getId());

        found.setName("Vasya");

        bookshelf.updateAuthor(found);

        Author result = bookshelf.getAuthor(found.getId());

        assertEquals("Vasya", result.getName());
    }

    @Test
    void deleteAuthor() {
        Author author = Author.create("abc");
        bookshelf.addAuthor(author);

        assertNotNull(bookshelf.getAuthor(author.getId()));

        bookshelf.deleteAuthor(author.getId());

        assertNull(bookshelf.getAuthor(author.getId()));
    }

    @Test
    void deleteBook() {
        Book book = Book.create("abc");
        bookshelf.addBook(book);

        assertNotNull(bookshelf.getBook(book.getId()));

        bookshelf.deleteBook(book.getId());

        assertNull(bookshelf.getBook(book.getId()));
    }

    @Test
    void updateBookAuthors() {
        Book book = Book.create("abc");
        bookshelf.addBook(book);

        Author author = Author.create("abc");
        bookshelf.addAuthor(author);

        bookshelf.updateBookAuthors(book.getId(), Collections.singletonList(author.getId()));

        Book result = bookshelf.getBook(book.getId());
        assertEquals(1, result.getAuthors().size());
    }

    @Test
    void addComment() {
        Book book = Book.create("abc");
        bookshelf.addBook(book);

        Comment comment = Comment.create("mycomment", book);
        bookshelf.addComment(comment);

        assertTrue(bookshelf.getBook(book.getId()).toString().contains("mycomment"));
    }

    @Test
    void getGenre() {
        Genre genre = Genre.create("John Gold");

        bookshelf.addGenre(genre);

        assertEquals(genre.getName(), bookshelf.getGenre(genre.getId()).getName());
    }

    @Test
    void getGenreWithBooks() {
        Book book = Book.create("John Gold", "novel");

        bookshelf.addBook(book);

        Genre genre = bookshelf.getBook(book.getId()).getGenre();
        genre = bookshelf.getGenre(genre.getId());


        assertEquals(genre.getName(), "novel");
        assertEquals(1, genre.getBooks().size());
    }

    @Test
    void testUpdateBooksInGenreDisabled() {
        Author author = Author.create("Ayn Rand");

        Book book1 = Book.create("The Fountainhead");
        book1.setGenre(Genre.create("Novel"));
        book1.setAuthors(Collections.singleton(author));

        bookshelf.addBook(book1);

        Book savedBook1 = bookshelf.getBook(book1.getId());

        assertEquals("Novel", savedBook1.getGenre().getName());

        Book book2 = Book.create("Atlas Shrugged");
        bookshelf.addBook(book2);
        Book savedBook2 = bookshelf.getBook(book2.getId());

        Genre genre = book1.getGenre();
        genre.setBooks(Collections.singleton(book2));

        bookshelf.addGenre(genre);
        savedBook2 = bookshelf.getBook(book2.getId());

        assertNull(savedBook2.getGenre());
    }

    @Test
    void testUpdateAuthorshipFromAuthorDisabled() {
        Author author = Author.create("Ayn Rand");

        Book book1 = Book.create("The Fountainhead");
        book1.setAuthors(Collections.singleton(author));

        bookshelf.addBook(book1);

        Book savedBook1 = bookshelf.getBook(book1.getId());

        Book book2 = Book.create("Atlas Shrugged");
        bookshelf.addBook(book2);
        Book savedBook2 = bookshelf.getBook(book2.getId());

        Author book1Author = savedBook1.getAuthors().stream().findAny().get();

        book1Author.setBooks(Collections.singleton(savedBook2));

        bookshelf.updateAuthor(book1Author);

        savedBook2 = bookshelf.getBook(book2.getId());
        book1Author = bookshelf.getAuthor(book1Author.getId());

        assertTrue(savedBook2.getAuthors().isEmpty());
    }

    @Test
    void testUpdateGenreNameFromBookUpdate() {
        Book book1 = Book.create("The Fountainhead");
        book1.setGenre(Genre.create("Novel"));

        bookshelf.addBook(book1);

        Book savedBook1 = bookshelf.getBook(book1.getId());
        Genre genre = savedBook1.getGenre();

        assertEquals("Novel", genre.getName());

        genre.setName("Song");
        savedBook1.setGenre(genre);

        bookshelf.updateBook(savedBook1);

        savedBook1 = bookshelf.getBook(book1.getId());

        assertEquals("Song", savedBook1.getGenre().getName());
    }
}