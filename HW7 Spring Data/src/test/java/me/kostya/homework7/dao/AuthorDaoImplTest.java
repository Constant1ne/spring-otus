package me.kostya.homework7.dao;

import me.kostya.homework7.dao.AuthorDao;
import me.kostya.homework7.entities.Author;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.shell.jline.InteractiveShellApplicationRunner;
import org.springframework.shell.jline.ScriptShellApplicationRunner;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = {
        InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED + "=false",
        ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT_ENABLED + "=false"
})
class AuthorDaoImplTest {

    @Autowired
    AuthorDao authorDao;

    @BeforeEach
    void setUp() {
        authorDao.deleteAll();
    }

    @Test
    void findByName() {
        Author vasya = Author.create("Vasya");
        authorDao.save(vasya);

        List<Author> test = authorDao.findByName(vasya.getName());
        assertEquals(vasya.getId(), test.get(0).getId());
    }

    @Test
    void findAll() {
        Author vasya = Author.create("Vasya");
        Author ivan = Author.create("Ivan");

        authorDao.save(vasya);
        authorDao.save(ivan);

        assertEquals(2, authorDao.findAll().size());
    }

    @Test
    void count() {
        Author vasya = Author.create("Vasya");
        authorDao.save(vasya);

        assertEquals(1, authorDao.count());
    }

    @Test
    void insert() {
        Author vasya = Author.create("Vasya");
        authorDao.save(vasya);

        Author test = authorDao.findById(vasya.getId());
        assertEquals(vasya.getName(), test.getName());
    }

    @Test
    void update() {
        Author vasya = Author.create("Vasya");
        authorDao.save(vasya);

        vasya.setName("Ivan");
        authorDao.save(vasya);

        Author test = authorDao.findById(vasya.getId());
        assertEquals(vasya.getName(), test.getName());
    }

    @Test
    void deleteById() {
        Author vasya = Author.create("Vasya");
        authorDao.save(vasya);

        authorDao.deleteById(vasya.getId());

        Author test = authorDao.findById(vasya.getId());
        assertNull(test);
    }
}