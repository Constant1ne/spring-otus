package me.kostya.homework7.servicies;

import me.kostya.homework7.entities.Author;
import me.kostya.homework7.entities.Book;
import me.kostya.homework7.entities.Comment;
import me.kostya.homework7.entities.Genre;

import java.util.List;

public interface BookshelfService {
    void addAuthor(Author author);
    void addGenre(Genre genre);
    void addBook(Book book);

    List<Author> getAllAuthors();
    List<Book> getAllBooks();
    List<Genre> getAllGenres();

    Book getBook(long id);
    Author getAuthor(long id);
    Genre getGenre(long id);

    void updateBook(Book book);
    void updateAuthor(Author author);

    void deleteAuthor(long id);
    void deleteBook(long id);

    void deleteAll();

    void updateBookAuthors(long bookId, List<Long> authorIds);

    void addComment(Comment comment);
}
