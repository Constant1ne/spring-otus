package me.kostya.homework7.servicies;

import me.kostya.homework7.dao.*;
import me.kostya.homework7.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class BookshelfServiceImpl implements BookshelfService{

    private BookDao bookDao;
    private AuthorDao authorDao;
    private GenreDao genreDao;
    private CommentDao commentDao;

    public BookshelfServiceImpl(@Autowired BookDao bookDao,
                                @Autowired AuthorDao authorDao,
                                @Autowired GenreDao genreDao,
                                @Autowired CommentDao commentDao) {
        this.bookDao = bookDao;
        this.authorDao = authorDao;
        this.genreDao = genreDao;
        this.commentDao = commentDao;
    }

    @Override
    public void addAuthor(Author author) {
        authorDao.save(author);
    }

    @Override
    public void addGenre(Genre genre) {
        genreDao.save(genre);
    }

    @Override
    public void addBook(Book book) {
        bookDao.save(book);
    }

    @Override
    public void addComment(Comment comment) {
        commentDao.save(comment);
    }

    @Override
    public List<Author> getAllAuthors() {
        return authorDao.findAll();
    }

    @Override
    public List<Book> getAllBooks() {
        return bookDao.findAll();
    }

    @Override
    public List<Genre> getAllGenres() {
        return genreDao.findAll();
    }

    @Override
    public Book getBook(long id) {
        return bookDao.findById(id);
    }

    @Override
    @Transactional
    public Author getAuthor(long id) {
        Author author = authorDao.findById(id);
        if(author != null) {
            author.setBooks(author.getBooks());
        }
        return author;
    }

    @Override
    @Transactional
    public Genre getGenre(long id) {
        Genre genre = genreDao.findById(id);
        if(genre != null) {
            genre.setBooks(genre.getBooks());
        }
        return genre;
    }

    @Override
    public void updateBook(Book book) {
        bookDao.save(book);
    }

    @Override
    public void updateAuthor(Author author) {
        authorDao.save(author);
    }

    @Override
    public void deleteAuthor(long id) {
        authorDao.deleteById(id);
    }

    @Override
    public void deleteBook(long id) {
        bookDao.deleteById(id);
    }

    @Override
    public void updateBookAuthors(long bookId, List<Long> authorIds) {
        Book book = bookDao.findById(bookId);

        Set<Author> authors = authorIds.stream()
                .map(Author::create)
                .collect(Collectors.toSet());

        book.setAuthors(authors);
        bookDao.save(book);
    }

    @Override
    public void deleteAll() {
        commentDao.deleteAll();
        bookDao.deleteAll();
        genreDao.deleteAll();
        authorDao.deleteAll();
    }
}
