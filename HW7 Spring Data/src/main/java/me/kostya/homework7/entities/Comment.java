package me.kostya.homework7.entities;

import javax.persistence.*;

@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String text;

    @ManyToOne
    private Book book;

    private Comment() {
    }

    public static Comment create(String text, Book book) {
        Comment comment = new Comment();
        comment.text = text;
        comment.book = book;
        return  comment;
    }

    public long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public Book getBook() {
        return book;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", text='" + text + '\'' +
                '}';
    }
}
