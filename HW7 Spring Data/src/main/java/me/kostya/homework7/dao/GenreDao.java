package me.kostya.homework7.dao;

import me.kostya.homework7.entities.Genre;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GenreDao extends JpaRepository<Genre, Long> {

    Genre findById(long Id);
}
