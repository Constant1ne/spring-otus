package me.kostya.homework7.dao;

import me.kostya.homework7.entities.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookDao extends JpaRepository<Book, Long> {

    Book findById(long Id);

}
