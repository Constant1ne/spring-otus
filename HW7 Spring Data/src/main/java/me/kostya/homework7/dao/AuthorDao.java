package me.kostya.homework7.dao;

import me.kostya.homework7.entities.Author;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AuthorDao extends JpaRepository<Author, Long> {

    Author findById(long Id);

    List<Author> findByName(String name);
}
