package me.kostya;

import me.kostya.services.TestService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
  public static void main(String[] args) {
    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/spring-context.xml");
    TestService service = context.getBean(TestService.class);

    service.runTest();
  }
}
