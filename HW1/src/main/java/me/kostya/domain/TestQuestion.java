package me.kostya.domain;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public class TestQuestion {
  private String questionText;
  private List<String> answers;
  private Set<Integer> correctAnswers;

  public static TestQuestion createQuestion(String questionText,
                                            List<String> answers,
                                            Set<Integer> correctAnswers) {
    TestQuestion testQuestion = new TestQuestion();
    testQuestion.questionText = questionText;
    testQuestion.answers = answers;
    testQuestion.correctAnswers = correctAnswers;
    return testQuestion;
  }

  public String getQuestionText() {
    return questionText;
  }

  public void setQuestionText(String questionText) {
    this.questionText = questionText;
  }

  public Collection<String> getAnswers() {
    return answers;
  }

  public void setAnswers(List<String> answers) {
    this.answers = answers;
  }

  public Collection<Integer> getCorrectAnswers() {
    return correctAnswers;
  }

  public void setCorrectAnswers(Set<Integer> correctAnswers) {
    this.correctAnswers = correctAnswers;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder()
            .append("\r\n\r\nВопрос: ")
            .append(questionText);

    for (int i = 0; i < answers.size(); i++) {
      sb.append("\r\n").append(i).append(") ").append(answers.get(i));
    }

    return sb.toString();
  }
}
