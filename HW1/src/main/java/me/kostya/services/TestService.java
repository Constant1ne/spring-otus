package me.kostya.services;

import me.kostya.domain.TestData;
import me.kostya.domain.TestQuestion;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class TestService {
  private TestReader testReader;

  private Scanner scan;

  public TestService(TestReader testReader) {
    this.testReader = testReader;
  }

  public void setInputStream(InputStream inputStream) {
    this.scan = new Scanner(inputStream);
  }

  public void runTest() {
    TestData test = testReader.readTest();

    System.out.println("Введите фамилию:");
    String surname = scan.nextLine();

    System.out.println("Введите имя:");
    String name = scan.nextLine();

    int scores = 0;
    for(TestQuestion question : test.getQuestions()) {
      System.out.println(question);
      Set<Integer> answers = readAnswers();

      if(question.getCorrectAnswers().equals(answers)) {
        scores++;
      }
    }

    System.out.println(String.format("%s %s вы набрали %d", surname, name, scores));
  }

  private Set<Integer> readAnswers() {
    String answer = scan.nextLine().trim();

    return Arrays.stream(answer.split(" "))
            .map(Integer::valueOf)
            .collect(Collectors.toSet());
  }

}
