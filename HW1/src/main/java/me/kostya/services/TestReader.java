package me.kostya.services;

import me.kostya.domain.TestData;

public interface TestReader {
  TestData readTest();
}
