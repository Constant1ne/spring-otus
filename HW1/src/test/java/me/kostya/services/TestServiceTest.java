package me.kostya.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TestServiceTest {

  private ByteArrayOutputStream outContent;
  private final PrintStream originalOut = System.out;

  private TestService testService;

  @BeforeEach
  void setUp() {
    TestReader testReader = new CsvTestReader("./src/test/resources/testfile.csv");

    outContent = new ByteArrayOutputStream();
    System.setOut(new PrintStream(outContent));

    testService = new TestService(testReader);
  }

  @AfterEach
  void tearDown() {
    System.setOut(originalOut);
  }

  @Test
  void runTestAllCorrect() {
    String userInput = "Pupkin\n" +
            "Vasya\n" +
            "0\n" +
            "0\n" +
            "1 3\n" +
            "2\n" +
            "0";

    InputStream inputStream = new ByteArrayInputStream(userInput.getBytes());
    testService.setInputStream(inputStream);

    testService.runTest();
    List<String> outputStrings = Arrays.asList(outContent.toString().split("\r\n"));
    assertEquals("Pupkin Vasya вы набрали 5", outputStrings.get(outputStrings.size() - 1));
  }

  @Test
  void runTest1Error() {
    String userInput = "Pupkin\n" +
            "Vasya\n" +
            "0\n" +
            "0\n" +
            "1 3\n" +
            "2\n" +
            "1";

    InputStream inputStream = new ByteArrayInputStream(userInput.getBytes());
    testService.setInputStream(inputStream);

    testService.runTest();
    List<String> outputStrings = Arrays.asList(outContent.toString().split("\r\n"));
    assertEquals("Pupkin Vasya вы набрали 4", outputStrings.get(outputStrings.size() - 1));
  }


  @Test
  void runTestMultipleChoiceError() {
    String userInput = "Pupkin\n" +
            "Vasya\n" +
            "0\n" +
            "0\n" +
            "1\n" +
            "2\n" +
            "0";

    InputStream inputStream = new ByteArrayInputStream(userInput.getBytes());
    testService.setInputStream(inputStream);

    testService.runTest();
    List<String> outputStrings = Arrays.asList(outContent.toString().split("\r\n"));
    assertEquals("Pupkin Vasya вы набрали 4", outputStrings.get(outputStrings.size() - 1));
  }
}