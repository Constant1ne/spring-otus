package me.kostya.services;

import me.kostya.domain.TestData;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CsvTestReaderTest {

  @Test
  void readTest() {
    TestReader testReader = new CsvTestReader("./src/test/resources/testfile.csv");
    TestData testData = testReader.readTest();

    assertEquals(5, testData.getQuestions().size());
  }
}