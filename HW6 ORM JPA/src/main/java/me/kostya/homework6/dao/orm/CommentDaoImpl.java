package me.kostya.homework6.dao.orm;

import me.kostya.homework6.dao.CommentDao;
import me.kostya.homework6.entities.Comment;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;


@Repository
@Transactional
public class CommentDaoImpl extends AbstractGenericDao<Comment> implements CommentDao {
    public CommentDaoImpl() {
        super(Comment.class);
    }
}
