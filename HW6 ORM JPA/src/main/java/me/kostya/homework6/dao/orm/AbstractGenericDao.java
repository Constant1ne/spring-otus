package me.kostya.homework6.dao.orm;

import me.kostya.homework6.dao.GenericDao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public abstract class AbstractGenericDao<T> implements GenericDao<T> {

    @PersistenceContext
    protected EntityManager em;

    private final Class<T> clazz;

    private final String findByNameJPQL;
    private final String findAllJPQL;
    private final String countJPQL;
    private final String deleteAll;

    public AbstractGenericDao(Class<T> clazz) {
        this.clazz = clazz;
        findByNameJPQL = "select a from " + clazz.getName() + " a where a.name = :name";
        findAllJPQL = "from " + clazz.getName();
        countJPQL = "select count(*) from " + clazz.getName();
        deleteAll = "delete from " + clazz.getName();
    }

    @Override
    public T findById(long id) {
        return em.find(clazz, id);
    }

    @Override
    public List<T> findByName(String name) {
        TypedQuery<T> query = em.createQuery(findByNameJPQL, clazz);
        query.setParameter("name", name);
        return query.getResultList();
    }

    @Override
    public List<T> findAll() {
        TypedQuery<T> query = em.createQuery(findAllJPQL, clazz);
        return query.getResultList();
    }

    @Override
    public long count() {
        TypedQuery<Long> query = em.createQuery(countJPQL, Long.class);
        return query.getSingleResult();
    }

    @Override
    public void insert(T o) {
        em.persist(o);
    }

    @Override
    public void update(T o) {
        em.merge(o);
    }

    @Override
    public void deleteById(long id) {
        em.remove(findById(id));
    }

    public void deleteAll() {
        em.createQuery(deleteAll).executeUpdate();
    }
}
