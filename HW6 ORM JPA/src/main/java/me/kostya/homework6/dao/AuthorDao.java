package me.kostya.homework6.dao;

import me.kostya.homework6.entities.Author;

public interface AuthorDao extends GenericDao<Author> {

}
