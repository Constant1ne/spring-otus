package me.kostya.homework6.dao.orm;


import me.kostya.homework6.dao.BookDao;
import me.kostya.homework6.entities.Book;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public class BookDaoImpl extends AbstractGenericDao<Book> implements BookDao {
    public BookDaoImpl() {
        super(Book.class);
    }
}
