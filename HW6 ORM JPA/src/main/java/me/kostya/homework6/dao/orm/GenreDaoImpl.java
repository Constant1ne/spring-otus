package me.kostya.homework6.dao.orm;

import me.kostya.homework6.dao.GenreDao;
import me.kostya.homework6.entities.Genre;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public class GenreDaoImpl extends AbstractGenericDao<Genre> implements GenreDao {
    public GenreDaoImpl() {
        super(Genre.class);
    }

    @Override
    public Genre findByIdWithBooks(long id) {
        Genre genre = em.find(Genre.class, id);
        genre.getBooks().size();
        return genre;
    }
}
