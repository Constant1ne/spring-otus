package me.kostya.homework6.dao;

import me.kostya.homework6.entities.Comment;

public interface CommentDao extends GenericDao<Comment> {
}
