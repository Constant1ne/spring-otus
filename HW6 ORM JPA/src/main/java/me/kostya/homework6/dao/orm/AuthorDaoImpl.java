package me.kostya.homework6.dao.orm;

import me.kostya.homework6.dao.AuthorDao;
import me.kostya.homework6.entities.Author;
import org.springframework.stereotype.Repository;


import javax.transaction.Transactional;

@Repository
@Transactional
public class AuthorDaoImpl extends AbstractGenericDao<Author> implements AuthorDao {
    public AuthorDaoImpl() {
        super(Author.class);
    }
}
