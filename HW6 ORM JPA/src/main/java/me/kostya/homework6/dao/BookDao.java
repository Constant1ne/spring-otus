package me.kostya.homework6.dao;

import me.kostya.homework6.entities.Book;

public interface BookDao extends GenericDao<Book>{

}
