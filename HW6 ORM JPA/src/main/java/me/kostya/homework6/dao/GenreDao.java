package me.kostya.homework6.dao;

import me.kostya.homework6.entities.Genre;

public interface GenreDao extends GenericDao<Genre>{
    Genre findByIdWithBooks(long id);
}
