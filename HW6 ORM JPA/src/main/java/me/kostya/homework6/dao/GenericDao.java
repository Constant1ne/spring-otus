package me.kostya.homework6.dao;

import java.util.List;

public interface GenericDao<T> {

    T findById(long id);

    List<T> findByName(String name);

    List<T> findAll();

    long count();

    void insert(T o);

    void update(T o);

    void deleteById(long id);

    void deleteAll();
}
