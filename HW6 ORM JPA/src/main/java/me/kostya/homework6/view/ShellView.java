package me.kostya.homework6.view;

import me.kostya.homework6.entities.Author;
import me.kostya.homework6.entities.Book;
import me.kostya.homework6.entities.Comment;
import me.kostya.homework6.entities.Genre;
import me.kostya.homework6.servicies.BookshelfService;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.util.Arrays;

@ShellComponent
public class ShellView {

    private BookshelfService bookshelf;

    public ShellView(BookshelfService bookshelf) {
        this.bookshelf = bookshelf;
    }

    @ShellMethod("View all authors")
    public String listAuthors() {
        StringBuilder sb = new StringBuilder();

        bookshelf.getAllAuthors().stream()
                .forEach(a -> sb.append(a.getId())
                        .append(" ")
                        .append(a.getName())
                        .append("\r\n"));

        return sb.toString();
    }

    @ShellMethod("View all genres")
    public String listGenres() {
        StringBuilder sb = new StringBuilder();

        bookshelf.getAllGenres().stream()
                .forEach(a -> sb.append(a.getId())
                        .append(" ")
                        .append(a.getName())
                        .append("\r\n"));

        return sb.toString();
    }

    @ShellMethod("View all books")
    public String listBooks() {
        StringBuilder sb = new StringBuilder();

        bookshelf.getAllBooks().stream()
                .forEach(book -> sb.append(book.getId())
                        .append(" ")
                        .append(book.getName())
                        .append(" ")
                        .append(book.getGenre().getName())
                        .append("\r\n"));

        return sb.toString();
    }

    @ShellMethod("Add author")
    public String addAuthor(String authorName) {
        Author author = Author.create(authorName);
        bookshelf.addAuthor(author);
        return "Author id is " + author.getId();
    }

    @ShellMethod("Add genre")
    public String addGenre(String genreName) {
        Genre genre = Genre.create(genreName);
        bookshelf.addGenre(genre);
        return "Genre id is " + genre.getId();
    }

    @ShellMethod("Add Book")
    public String addBook(String bookName, String genreName) {
        Book book = Book.create(bookName, genreName);
        bookshelf.addBook(book);
        return "Book id is " + book.getId();
    }

    @ShellMethod("Update Book")
    public String updateBook(long bookId, String bookName, String genreName) {
        try {
            bookshelf.updateBook(Book.create(bookId, bookName, Genre.create(genreName)));
            return "OK";
        } catch (Exception ex) {
            return "Error";
        }
    }

    @ShellMethod("Update Author")
    public String updateAuthor(long authorId, String authorName) {
        try {
            bookshelf.updateAuthor(Author.create(authorId, authorName));
            return "OK";
        } catch (Exception ex) {
            return "Error";
        }
    }

    @ShellMethod("List Book Authors")
    public String listBookAuthors(long bookId) {
        StringBuilder sb = new StringBuilder();
        bookshelf.getBook(bookId).getAuthors().stream()
                .forEach(a -> sb.append(a.getId())
                        .append(" ")
                        .append(a.getName())
                        .append("\r\n"));

        return sb.toString();
    }

    @ShellMethod("Set Book Authors")
    public String setBookAuthors(long bookId, Long ... authorIds) {
        bookshelf.updateBookAuthors(bookId, Arrays.asList(authorIds));
        return "OK";
    }

    @ShellMethod("Delete Book")
    public String deleteBook(long bookId) {
        try {
            bookshelf.deleteBook(bookId);
            return "OK";
        } catch (Exception ex) {
            return "Error";
        }
    }

    @ShellMethod("List all books of genre")
    public String getAllBooksForGenre(long genreId) {
        Genre genre = bookshelf.getGenre(genreId);

        StringBuilder b = new StringBuilder();
        genre.getBooks().forEach(b::append);

        return b.toString();
    }

    @ShellMethod("View book")
    public String getBook(long bookId) {
        return bookshelf.getBook(bookId).toString();
    }

    @ShellMethod("Add comment")
    public String addComment(long bookId, String commentText) {
        Book book = bookshelf.getBook(bookId);
        Comment comment = Comment.create(commentText, book);

        bookshelf.addComment(comment);

        return "Comment id is " + comment.getId();
    }
}
