package me.kostya.homework6.servicies;

import me.kostya.homework6.dao.*;
import me.kostya.homework6.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class BookshelfServiceImpl implements BookshelfService{

    private BookDao bookDao;
    private AuthorDao authorDao;
    private GenreDao genreDao;
    private CommentDao commentDao;

    public BookshelfServiceImpl(@Autowired BookDao bookDao,
                                @Autowired AuthorDao authorDao,
                                @Autowired GenreDao genreDao,
                                @Autowired CommentDao commentDao) {
        this.bookDao = bookDao;
        this.authorDao = authorDao;
        this.genreDao = genreDao;
        this.commentDao = commentDao;
    }

    @Override
    public void addAuthor(Author author) {
        authorDao.insert(author);
    }

    @Override
    public void addGenre(Genre genre) {
        genreDao.insert(genre);
    }

    @Override
    public void addBook(Book book) {
        book.setGenre(getOrCreate(book.getGenre(), genreDao));
        bookDao.insert(book);
    }

    @Override
    public void addComment(Comment comment) {
        commentDao.insert(comment);
    }

    @Override
    public List<Author> getAllAuthors() {
        return authorDao.findAll();
    }

    @Override
    public List<Book> getAllBooks() {
        return bookDao.findAll();
    }

    @Override
    public List<Genre> getAllGenres() {
        return genreDao.findAll();
    }

    @Override
    public Book getBook(long id) {
        return bookDao.findById(id);
    }

    @Override
    public Author getAuthor(long id) {
        return authorDao.findById(id);
    }

    @Override
    public Genre getGenre(long id) {
        return genreDao.findByIdWithBooks(id);
    }

    @Override
    public void updateBook(Book book) {
        book.setGenre(getOrCreate(book.getGenre(), genreDao));
        bookDao.update(book);
    }

    @Override
    public void updateAuthor(Author author) {
        authorDao.update(author);
    }

    @Override
    public void deleteAuthor(long id) {
        authorDao.deleteById(id);
    }

    @Override
    public void deleteBook(long id) {
        bookDao.deleteById(id);
    }

    @Override
    public void updateBookAuthors(long bookId, List<Long> authorIds) {
        Book book = bookDao.findById(bookId);

        Set<Author> authors = authorIds.stream()
                .map(Author::create)
                .map(author -> getOrCreate(author, authorDao))
                .collect(Collectors.toSet());

        book.setAuthors(authors);
        bookDao.update(book);
    }

    @Override
    public void deleteAll() {
        commentDao.deleteAll();
        bookDao.deleteAll();
        genreDao.deleteAll();
        authorDao.deleteAll();
    }


    /**
     * Костыль, но нормального решения не удалось нагуглить.
     * Подскажите как не плодить дубли жанров и авторов при добавлении новых книг(с авторами и жанрами).
     */
    private <T extends FindOrInsertAllowed> T getOrCreate(T obj, GenericDao<T> dao) {
        if(obj == null) return null;

        T founded = dao.findById(obj.getId());
        if(founded != null) return founded;


        List<T> genres = dao.findByName(obj.getName());
        if(genres.isEmpty()) {
            dao.insert(obj);
            return obj;
        } else {
            return genres.get(0);
        }
    }
}
