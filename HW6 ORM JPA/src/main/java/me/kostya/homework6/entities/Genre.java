package me.kostya.homework6.entities;

import javax.persistence.*;
import java.util.Collections;
import java.util.Set;

@Entity
public class Genre implements FindOrInsertAllowed {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //@OneToMany(targetEntity = Book.class, cascade = CascadeType.ALL, orphanRemoval = true)
    private long id;
    @Column(unique = true)
    private String name;
    @OneToMany(targetEntity = Book.class, mappedBy = "genre")
    private Set<Book> books;

    public static Genre create(long id) {
        return create(id, null);
    }

    public static Genre create(String name) {
        return create(0, name);
    }

    public static Genre create(long id, String name) {
        Genre genre = new Genre();
        genre.id = id;
        genre.name = name;
        return genre;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Book> getBooks() {
        return Collections.unmodifiableSet(books);
    }

    @Override
    public String toString() {
        return "Genre{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
