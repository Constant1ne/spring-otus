package me.kostya.homework6.entities;


public interface FindOrInsertAllowed {
    long getId();
    String getName();
}
