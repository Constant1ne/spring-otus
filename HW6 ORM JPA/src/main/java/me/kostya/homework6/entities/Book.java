package me.kostya.homework6.entities;

import javax.persistence.*;
import java.util.*;

@Entity
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;

    @ManyToOne(targetEntity = Genre.class)
    private Genre genre;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Author> authors;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "book")
    private List<Comment> comments;

    public static Book create(String name) {
        return create(0, name, null);
    }

    public static Book create(String name, String genreName) {
        return create(0, name, Genre.create(genreName));
    }

    public static Book create(long id, String name, Genre genre) {
        Book book = new Book();
        book.setId(id);
        book.setName(name);
        book.setGenre(genre);
        book.authors = new HashSet<>();
        book.comments = new ArrayList<>();
        return book;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Set<Author> getAuthors() {
        return Collections.unmodifiableSet(authors);
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    public List<Comment> getComments() {
        return Collections.unmodifiableList(comments);
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", genre=" + genre +
                ", authors=" + authors +
                ", comments=" + comments +
                '}';
    }
}
