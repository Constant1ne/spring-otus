insert into BOOKSHELF.AUTHORS (name) VALUES ('Dostoevskiy');
insert into BOOKSHELF.AUTHORS (name) VALUES ('Tolstoy');
insert into BOOKSHELF.AUTHORS (name) VALUES ('Pyshkin');
insert into BOOKSHELF.AUTHORS (name) VALUES ('Turgeniev');


insert into BOOKSHELF.GENRES (name) values ('novel');
insert into BOOKSHELF.GENRES (name) values ('poetry');
insert into BOOKSHELF.GENRES (name) values ('satire');



insert into BOOKSHELF.BOOKS (name, genreId) values ('War and Peace', 1);
insert into BOOKSHELF.BOOKS (name, genreId) values ('Crime and Punishment', 1);
insert into BOOKSHELF.BOOKS (name, genreId) values ('Eugene Onegin', 2);
insert into BOOKSHELF.BOOKS (name, genreId) values ('aliens vs zombies', 2);


insert into BOOKSHELF.AUTHORSHIP (bookId, authorId) values (1, 2);
insert into BOOKSHELF.AUTHORSHIP (bookId, authorId) values (2, 1);
insert into BOOKSHELF.AUTHORSHIP (bookId, authorId) values (3, 3);

insert into BOOKSHELF.AUTHORSHIP (bookId, authorId) values (4, 1);
insert into BOOKSHELF.AUTHORSHIP (bookId, authorId) values (4, 2);
insert into BOOKSHELF.AUTHORSHIP (bookId, authorId) values (4, 3);