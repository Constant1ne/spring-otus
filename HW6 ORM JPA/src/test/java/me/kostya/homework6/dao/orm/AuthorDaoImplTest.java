package me.kostya.homework6.dao.orm;

import me.kostya.homework6.entities.Author;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.shell.jline.InteractiveShellApplicationRunner;
import org.springframework.shell.jline.ScriptShellApplicationRunner;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = {
        InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED + "=false",
        ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT_ENABLED + "=false"
})
class AuthorDaoImplTest {

    @Autowired
    AuthorDaoImpl authorDao;

    @BeforeEach
    void setUp() {
        authorDao.deleteAll();
    }

    @Test
    void findByName() {
        Author vasya = Author.create("Vasya");
        authorDao.insert(vasya);

        List<Author> test = authorDao.findByName(vasya.getName());
        assertEquals(vasya.getId(), test.get(0).getId());
    }

    @Test
    void findAll() {
        Author vasya = Author.create("Vasya");
        Author ivan = Author.create("Ivan");

        authorDao.insert(vasya);
        authorDao.insert(ivan);

        assertEquals(2, authorDao.findAll().size());
    }

    @Test
    void count() {
        Author vasya = Author.create("Vasya");
        authorDao.insert(vasya);

        assertEquals(1, authorDao.count());
    }

    @Test
    void insert() {
        Author vasya = Author.create("Vasya");
        authorDao.insert(vasya);

        Author test = authorDao.findById(vasya.getId());
        assertEquals(vasya.getName(), test.getName());
    }

    @Test
    void update() {
        Author vasya = Author.create("Vasya");
        authorDao.insert(vasya);

        vasya.setName("Ivan");
        authorDao.update(vasya);

        Author test = authorDao.findById(vasya.getId());
        assertEquals(vasya.getName(), test.getName());
    }

    @Test
    void deleteById() {
        Author vasya = Author.create("Vasya");
        authorDao.insert(vasya);

        authorDao.deleteById(vasya.getId());

        Author test = authorDao.findById(vasya.getId());
        assertNull(test);
    }
}