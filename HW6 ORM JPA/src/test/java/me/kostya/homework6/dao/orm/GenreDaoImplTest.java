package me.kostya.homework6.dao.orm;

import me.kostya.homework6.dao.BookDao;
import me.kostya.homework6.entities.Book;
import me.kostya.homework6.entities.Genre;
import me.kostya.homework6.servicies.BookshelfService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.shell.jline.InteractiveShellApplicationRunner;
import org.springframework.shell.jline.ScriptShellApplicationRunner;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = {
        InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED + "=false",
        ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT_ENABLED + "=false"
})
class GenreDaoImplTest {

    @Autowired
    GenreDaoImpl genreDao;

    @Autowired
    BookshelfService bookshelf;

    @BeforeEach
    void setUp() {
        genreDao.deleteAll();
    }

    @Test
    void findByIdWithBooks() {
        Genre genre = Genre.create("abc");
        genreDao.insert(genre);

        Book book = Book.create(0,"zzz",genre);
        bookshelf.addBook(book);

        Genre result = genreDao.findByIdWithBooks(genre.getId());
        assertEquals(1, result.getBooks().size());
    }
}