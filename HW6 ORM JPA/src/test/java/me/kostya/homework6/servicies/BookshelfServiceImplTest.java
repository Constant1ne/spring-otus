package me.kostya.homework6.servicies;

import me.kostya.homework6.entities.Author;
import me.kostya.homework6.entities.Book;
import me.kostya.homework6.entities.Comment;
import me.kostya.homework6.entities.Genre;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.shell.jline.InteractiveShellApplicationRunner;
import org.springframework.shell.jline.ScriptShellApplicationRunner;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = {
        InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED + "=false",
        ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT_ENABLED + "=false"
})
class BookshelfServiceImplTest {

    @Autowired
    BookshelfService bookshelf;

    @BeforeEach
    void setUp() {
        bookshelf.deleteAll();
    }

    @Test
    void addAuthor() {
        Author author = new Author();
        author.setName("Gogol");

        bookshelf.addAuthor(author);

        assertEquals(author.getName(), bookshelf.getAuthor(author.getId()).getName());
    }

    @Test
    void addGenre() {
        Genre genre = new Genre();
        genre.setName("Novel");

        bookshelf.addGenre(genre);

        assertTrue(genre.getId() > 0);
    }

    @Test
    void addBook() {
        Book book = Book.create("John Gold");

        bookshelf.addBook(book);

        assertEquals(book.getName(), bookshelf.getBook(book.getId()).getName());
    }

    @Test
    void getAllAuthors() {
        bookshelf.addAuthor(Author.create("Vasya"));
        bookshelf.addAuthor(Author.create("Ivan"));
        assertEquals(2, bookshelf.getAllAuthors().size());
    }

    @Test
    void getAllBooks() {
        bookshelf.addBook(Book.create("abc"));
        bookshelf.addBook(Book.create("qwe"));
        assertEquals(2, bookshelf.getAllBooks().size());
    }

    @Test
    void getAllGenres() {
        bookshelf.addGenre(Genre.create("zxc"));
        bookshelf.addGenre(Genre.create("fgh"));
        assertEquals(2, bookshelf.getAllGenres().size());
    }

    @Test
    void getBook() {
        Book book = Book.create("abc");
        bookshelf.addBook(book);
        assertEquals("abc", bookshelf.getBook(book.getId()).getName());
    }

    @Test
    void getAuthor() {
        Author author = Author.create("abc");
        bookshelf.addAuthor(author);
        assertEquals("abc", bookshelf.getAuthor(author.getId()).getName());
    }

    @Test
    void updateBook() {
        Book book = Book.create("abc");
        bookshelf.addBook(book);

        Book result = bookshelf.getBook(book.getId());

        result.setName("John Gold");
        result.setGenre(Genre.create("asd"));

        bookshelf.updateBook(result);

        Book check = bookshelf.getBook(result.getId());

        assertEquals("John Gold", check.getName());
        assertEquals("asd", check.getGenre().getName());
    }

    @Test
    void updateAuthor() {
        Author author = Author.create("abc");
        bookshelf.addAuthor(author);

        Author found = bookshelf.getAuthor(author.getId());

        found.setName("Vasya");

        bookshelf.updateAuthor(found);

        Author result = bookshelf.getAuthor(found.getId());

        assertEquals("Vasya", result.getName());
    }

    @Test
    void deleteAuthor() {
        Author author = Author.create("abc");
        bookshelf.addAuthor(author);

        assertNotNull(bookshelf.getAuthor(author.getId()));

        bookshelf.deleteAuthor(author.getId());

        assertNull(bookshelf.getAuthor(author.getId()));
    }

    @Test
    void deleteBook() {
        Book book = Book.create("abc");
        bookshelf.addBook(book);

        assertNotNull(bookshelf.getBook(book.getId()));

        bookshelf.deleteBook(book.getId());

        assertNull(bookshelf.getBook(book.getId()));
    }

    @Test
    void updateBookAuthors() {
        Book book = Book.create("abc");
        bookshelf.addBook(book);

        Author author = Author.create("abc");
        bookshelf.addAuthor(author);

        bookshelf.updateBookAuthors(book.getId(), Collections.singletonList(author.getId()));

        Book result = bookshelf.getBook(book.getId());
        assertEquals(1, result.getAuthors().size());
    }

    @Test
    void addComment() {
        Book book = Book.create("abc");
        bookshelf.addBook(book);

        Comment comment = Comment.create("mycomment", book);
        bookshelf.addComment(comment);

        assertTrue(bookshelf.getBook(book.getId()).toString().contains("mycomment"));
    }

    @Test
    void getGenre() {
        Genre genre = Genre.create("John Gold");

        bookshelf.addGenre(genre);

        assertEquals(genre.getName(), bookshelf.getGenre(genre.getId()).getName());
    }
}