package me.kostya.homework16.dao;

import me.kostya.homework16.entities.Book;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "/hateoasbooks")
public interface HateOasAndBooksDao extends MongoRepository<Book, String> {
}
