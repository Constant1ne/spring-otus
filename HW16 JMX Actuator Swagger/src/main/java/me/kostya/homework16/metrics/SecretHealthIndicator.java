package me.kostya.homework16.metrics;

import me.kostya.homework16.servicies.BookshelfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Service;

@Service
public class SecretHealthIndicator implements HealthIndicator {

    @Autowired
    BookshelfService bookshelfService;

    @Override
    public Health health() {
        return Health.up().status(String.valueOf(bookshelfService.getSecretCounter())).build();
    }
}
