package me.kostya.homework16.metrics;

import me.kostya.homework16.servicies.BookshelfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

@Service
@ManagedResource()
public class SecretMbean {
    @Autowired
    BookshelfService bookshelfService;

    @ManagedAttribute
    public long getSecretCounter() {
        return bookshelfService.getSecretCounter();
    }
}
