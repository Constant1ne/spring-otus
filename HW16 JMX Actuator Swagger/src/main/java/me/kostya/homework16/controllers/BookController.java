package me.kostya.homework16.controllers;

import me.kostya.homework16.servicies.BookshelfService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class BookController {
    private final BookshelfService bookshelfService;



    public BookController(@Autowired BookshelfService bookshelfService) {
        this.bookshelfService = bookshelfService;
    }


    @GetMapping("/secretcounter")
    public long secretCounter() {
        return bookshelfService.incrementAndGetSecretCounter();
    }
}
