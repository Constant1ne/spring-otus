package me.kostya.homework16.servicies;

import me.kostya.homework16.dao.*;
import me.kostya.homework16.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class BookshelfServiceImpl implements BookshelfService {

    private AtomicLong secretCounter = new AtomicLong();

    @Override
    public long incrementAndGetSecretCounter() {
        return secretCounter.incrementAndGet();
    }

    @Override
    public long getSecretCounter() {
        return secretCounter.get();
    }
}
