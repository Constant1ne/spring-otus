package me.kostya.homework16.servicies;

import me.kostya.homework16.entities.*;

import java.util.List;

public interface BookshelfService {
    long incrementAndGetSecretCounter();
    long getSecretCounter();
}
