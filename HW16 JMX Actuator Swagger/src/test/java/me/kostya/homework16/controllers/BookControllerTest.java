package me.kostya.homework16.controllers;


import me.kostya.homework16.servicies.BookshelfService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;


import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(secure = false)
class BookControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private BookshelfService bookshelfService;


    @BeforeEach
    void setUp() {
    }


    @Test
    void secretCounter() throws Exception {
        given(bookshelfService.incrementAndGetSecretCounter())
                .willReturn(1L);

        mvc.perform(get("/api/secretcounter").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().string("1"));
    }
}