package me.kostya.homework10.view;

import me.kostya.homework10.entities.*;
import me.kostya.homework10.servicies.BookshelfService;

import java.util.Arrays;
import java.util.stream.Collectors;

public class ShellView {

    private BookshelfService bookshelf;

    public ShellView(BookshelfService bookshelf) {
        this.bookshelf = bookshelf;
    }

    public String listAuthors() {
        return bookshelf.getAllAuthors().stream()
                .map(Author::toString)
                .collect(Collectors.joining("\r\n"));
    }

    public String listGenres() {
        return bookshelf.getAllGenres().stream()
                .map(Genre::toString)
                .collect(Collectors.joining("\r\n"));
    }

    public String listBooks() {
        return bookshelf.getAllBooks().stream()
                .map(Book::toString)
                .collect(Collectors.joining("\r\n"));
    }

    public String addBook(String bookName, String genreName) {
        Book book = Book.create(bookName, genreName);
        book = bookshelf.saveBook(book);
        return "Book id is " + book.getId();
    }

    public String updateBook(String bookId, String bookName, String genreName) {
        try {
            bookshelf.saveBook(Book.create(bookId, bookName, Genre.create(genreName)));
            return "OK";
        } catch (Exception ex) {
            return "Error";
        }
    }

    public String listBookAuthors(String bookId) {
        return bookshelf.getBook(bookId).getAuthors().stream()
                .map(Author::toString)
                .collect(Collectors.joining("\r\n"));
    }

    public String setBookAuthors(String bookId, String... authorIds) {
        bookshelf.updateBookAuthors(bookId, Arrays.asList(authorIds));
        return "OK";
    }

    public String deleteBook(String bookId) {
        try {
            bookshelf.deleteBook(bookId);
            return "OK";
        } catch (Exception ex) {
            return "Error";
        }
    }

    public String getAllBooksForGenre(String genreName) {
        return bookshelf.getBooksOfGenre(genreName).stream()
                .map(Book::toString)
                .collect(Collectors.joining("\r\n"));
    }

    public String getBook(String bookId) {
        try {
            return bookshelf.getBook(bookId).toString();
        } catch (Exception ex) {
            return "ERROR";
        }
    }

    public String addComment(String bookId, String commentText) {
        try {
            bookshelf.addComment(bookId, commentText);
            return "OK";
        } catch (Exception ex) {
            return "ERROR";
        }
    }

    public String addAuthor(String bookId, String authorName) {
        try {
            Book book = bookshelf.addAuthor(bookId, authorName);
            return book.toString();
        } catch (Exception ex) {
            return "ERROR";
        }
    }

    public String setGenre(String bookId, String genreName) {
        try {
            Book book = bookshelf.setGenre(bookId, genreName);
            return book.toString();
        } catch (Exception ex) {
            return "ERROR";
        }
    }
}
