package me.kostya.homework10.dao;

import me.kostya.homework10.entities.Author;
import me.kostya.homework10.entities.Genre;

import java.util.List;

public interface CustomBookDao {
    List<Genre> findAllGenres();
    List<Author> findAllAuthors();
}
