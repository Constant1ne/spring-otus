import React, {Component} from 'react';
import './App.css';

import BookList from "./components/BookList";
import Book from "./components/Book";
import {Route, BrowserRouter} from "react-router-dom";

class App extends Component {
    render() {
        return (
            <div className="App">
                <BrowserRouter>
                    <div>
                        <Route exact path="/" component={BookList}/>
                        <Route path="/create" component={Book}/>
                        <Route path="/book/:id" component={Book}/>
                    </div>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;
