import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import Button from "@material-ui/core/Button/Button";
import SaveIcon from '@material-ui/icons/Save';
import FormControl from "@material-ui/core/FormControl/FormControl";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Input from "@material-ui/core/Input/Input";
import FormHelperText from "@material-ui/core/FormHelperText/FormHelperText";
import Paper from "@material-ui/core/Paper/Paper";
import {Redirect} from 'react-router-dom'

const styles = theme => ({
    root: {
        margin: theme.spacing.unit * 4,
        overflowX: 'auto',
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
    },
});

class Book extends React.Component {

    state = {
        id: null, name: "", genre: {name: ""}, authorsAsString: "", comments: []
    };

    componentDidMount() {
        if(this.props.match.params.id) {
            this.fetchBookToState(this.props.match.params.id);
        }
    }

    render() {
        const {classes} = this.props;

        return (
            <Paper className={classes.root}>

                {this.state.redirect && <Redirect to='/'/>}

                <div className={classes.container}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="book-name">Название</InputLabel>
                        <Input id="book-name" value={this.state.name} onChange={this.handleChange('name')}/>
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="book-name">Жанр</InputLabel>
                        <Input id="book-name" value={this.state.genre.name} onChange={event => {
                            this.setState({genre: {name: event.target.value}});
                        }}/>
                    </FormControl>
                    <FormControl className={classes.formControl} aria-describedby="author-helper-text">
                        <InputLabel htmlFor="authors">Авторы</InputLabel>
                        <Input id="authors" value={this.state.authorsAsString}
                               onChange={this.handleChange('authorsAsString')}/>
                        <FormHelperText id="author-helper-text">перечислять через ,</FormHelperText>
                    </FormControl>
                    <Button variant="contained" color="primary" aria-label="Save"
                            onClick={() => {
                                this.saveBook(this.state);
                            }}>
                        <SaveIcon/>
                        Сохранить
                    </Button>
                </div>
            </Paper>
        );
    }

    fetchBookToState(bookId) {
        fetch('/api/read/'+bookId, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "GET"
        })
            .then(response => response.json())
            .then(data => this.setState(data));
    }

    saveBook(book) {
        fetch('/api/update/', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: Book.prepareToSend(book)
        })
            .then(() => this.setState({redirect: true}));
    }

    static prepareToSend(book) {
        book.authors = book.authorsAsString.split(',').map(str => str.trim()).map(str => ({name: str}));
        return JSON.stringify(book);
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };
}


export default withStyles(styles)(Book);