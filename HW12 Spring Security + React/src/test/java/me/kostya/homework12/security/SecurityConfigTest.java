package me.kostya.homework12.security;

import me.kostya.homework12.controllers.BookController;
import me.kostya.homework12.servicies.BookshelfService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(BookController.class)
@Import(SecurityConfig.class)
public class SecurityConfigTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private BookshelfService bookshelfService;

    @WithMockUser("admin")
    @Test
    public void springSecurity1() throws Exception {
        mvc.perform(delete("/api/delete").param("id","123"))
                .andExpect(status().isOk());
    }

    @Test
    public void springSecurity2() throws Exception {
        mvc.perform(delete("/api/delete").param("id","123"))
                .andExpect(status().is(302));
    }
}