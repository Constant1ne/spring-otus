import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from "@material-ui/core/Button/Button";
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from "@material-ui/icons/Add";
import {Link} from 'react-router-dom'

const styles = theme => ({
    root: {
        margin: theme.spacing.unit * 4,
        overflowX: 'auto',
    },
    table: {
        minWidth: 630,
    },
    button: {
        margin: theme.spacing.unit,
    }
});

const LinkToCreateBook = (props) => <Link to={'/create'} {...props}/>;
const getLinkToEditBook = (bookId) => (props) => <Link to={'/book/' + bookId} {...props}/>;

class BookList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            books: []
        };
    }

    componentDidMount() {
        this.fetchBookListToState();
    }

    render() {
        const {classes} = this.props;

        return (
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Название</TableCell>
                            <TableCell>Жанр</TableCell>
                            <TableCell>Авторы</TableCell>
                            <TableCell>Инструменты</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.books.map(book => {
                            return (
                                <TableRow key={book.id}>
                                    <TableCell component="th" scope="row">
                                        <Link to={'/book/' + book.id}>
                                            {book.name}
                                        </Link>
                                    </TableCell>
                                    <TableCell>{book.genre.name}</TableCell>
                                    <TableCell>{book.authorsAsString}</TableCell>
                                    <TableCell>
                                        <Button variant="fab" color="primary" aria-label="Edit"
                                                component={getLinkToEditBook(book.id)}
                                                className={classes.button}>
                                            <EditIcon/>
                                        </Button>
                                        {this.props.isAuthenticated &&
                                        <Button variant="fab" color="secondary" aria-label="Delete"
                                                onClick={() => this.deleteBook(book.id)}
                                                className={classes.button}>
                                            <DeleteIcon/>
                                        </Button>}
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
                <Button variant="contained" color="primary" aria-label="Add" component={LinkToCreateBook}>
                    <AddIcon/>
                    Добавить книгу
                </Button>
            </Paper>
        );
    }

    fetchBookListToState() {
        fetch('/api/', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "GET"
        })
            .then(response => response.json())
            .then(data => this.setState({books: data}));
    }

    deleteBook(bookId) {
        fetch('/api/delete?id=' + bookId, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "DELETE"
        })
            .then(() => this.fetchBookListToState());
    }
}

BookList.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(BookList);