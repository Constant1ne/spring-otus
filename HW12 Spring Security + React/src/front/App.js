import React, {Component} from 'react';
import {Redirect} from 'react-router-dom'
import './App.css';

import BookList from "./components/BookList";
import Book from "./components/Book";
import {Route, BrowserRouter} from "react-router-dom";
import Login from "./components/Login";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAuthenticated: false
        }
    }

    render() {

        const PrivateRoute = ({component: Component, ...rest}) => (
            <Route {...rest} render={(props) => (
                this.state.isAuthenticated === true
                    ? <Component {...props} />
                    : <Redirect to='/login'/>
            )}/>
        );

        const withAuthInfo = (Component) => {
            return (props) => {
                return (<Component authenticateUser={() => this.setState({isAuthenticated: true})}
                                   isAuthenticated={this.state.isAuthenticated} {...props}/>)
            }
        };


        return (
            <div className="App">
                <BrowserRouter>
                    <div>
                        <Route exact path="/" component={withAuthInfo(BookList)}/>
                        <PrivateRoute path='/create' component={withAuthInfo(Book)}/>
                        <PrivateRoute path='/book/:id' component={withAuthInfo(Book)}/>
                        <Route path="/login" component={withAuthInfo(Login)}/>
                    </div>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;
