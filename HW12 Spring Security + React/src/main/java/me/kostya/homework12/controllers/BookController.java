package me.kostya.homework12.controllers;

import me.kostya.homework12.entities.Book;
import me.kostya.homework12.servicies.BookshelfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class BookController {
    private final BookshelfService bookshelfService;

    public BookController(@Autowired BookshelfService bookshelfService) {
        this.bookshelfService = bookshelfService;
    }

    @GetMapping("/")
    public List<Book> list() {
        return bookshelfService.getAllBooks();
    }

    @GetMapping("/read/{id}")
    public Book read(@PathVariable("id") String id) {
        return bookshelfService.getBook(id);
    }

    @PostMapping("/update")
    public Book update(@RequestBody Book book) {
        return bookshelfService.saveBook(book);
    }

    @DeleteMapping("/delete")
    public Boolean delete(@RequestParam("id") String id) {
        bookshelfService.deleteBook(id);
        return true;
    }
}
