package me.kostya.homework12.dao;

import me.kostya.homework12.entities.Author;
import me.kostya.homework12.entities.Genre;

import java.util.List;

public interface CustomBookDao {
    List<Genre> findAllGenres();
    List<Author> findAllAuthors();
}
